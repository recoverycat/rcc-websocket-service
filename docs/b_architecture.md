# Architecture - In progress

[**_Main Menu_**](../README.md)

## Decomposition

![decomposition.drawio.svg](assets%2Fdecomposition.drawio.svg)

## Uses

## Process

### Stream process

![streams.drawio.svg](assets%2Fstreams.drawio.svg)

### Generic Data Transfer

This following sequence diagram describes how two parties or clients interact with each other through the websocket service in order to transfer specific data for their needs. This diagram is an abstraction of the generic process to transfer data.

![websocket_comm_general_data_transfer_process.drawio.svg](assets%2Fwebsocket_comm_general_data_transfer_process.drawio.svg)

### Signaling Process

This following sequence diagram describes how two parties or clients interact with each other through the websocket service in order to transfer specific data for their needs. This diagram pays especial attention to the overall back and forth between these clients during the signal process when establishing a webrtc connection.

![websocket_comm_webrtc_signaling_process.drawio.svg](assets%2Fwebsocket_comm_webrtc_signaling_process.drawio.svg)

