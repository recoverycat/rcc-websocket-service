# Deployment Strategy

[**_Main Menu_**](../README.md)

## Deployment Strategy in combination with rcc-client.

Please note that the order in the tables represents a significant sequence of an artifact.

The following table shows the status of the websocket server on the rcc-client on main, when there has been no breaking
changes on the rcc-websocket service.

| # | Stage (branch)         | Rcc-websocket                  | Rcc-websocket Protocol Hint   | Comment                            |
|---|------------------------|--------------------------------|-------------------------------|------------------------------------|
| 1 | Rcc-client DEV (main)  | wss://signaling.recoverycat.de | Latest stable version: e.g V1 | Rcc-client evolves as usual.       |
| 2 | Rcc-client QA (main)   | wss://signaling.recoverycat.de | Latest stable version: e.g V1 | E2E tests and manual tests         |
| 3 | Rcc-client Prod (main) | wss://signaling.recoverycat.de | Latest stable version: e.g V1 | Rcc-client get officially released |

The following table shows the status of the websocket server as an independent artifact.

| # | Stage (branch)     | Rcc-websocket                      | Comment                                               |
|---|--------------------|------------------------------------|-------------------------------------------------------|
| 1 | Rcc-websocket DEV  | wss://signaling.dev.recoverycat.de | Normal development cycle                              |
| 2 | Rcc-websocket QA   | wss://signaling.qa.recoverycat.de  | Quality Assurance && next release distribution system |
| 3 | Rcc-websocket Prod | wss://signaling.recoverycat.de     | Productive system                                     |

The following table shows the status of the websocket server on the rcc-client, when there has been breaking changes
introduced on the rcc-websocket service.

| # | Stage (branch)                  | Rcc-websocket                                                | Rcc-websocket Protocol Hint   | Comment                                                                 |
|---|---------------------------------|--------------------------------------------------------------|-------------------------------|-------------------------------------------------------------------------|
| 1 | Rcc-client DEV (feature-branch) | wss://signaling.qa.recoverycat.de                            | Next release version: e.g V2  | Breaking changes integration.                                           |
| 2 | Rcc-client QA (feature-branch)  | wss://signaling.qa.recoverycat.de                            | Next release version: e.g V2  | E2E tests and manual tests                                              |
| 3 |                                 | **AFTER RCC-WEBSOCKET HAS BEEN OFFICIALLY DEPLOYED TO PROD** |                               | The rcc-websocket announces next version release to production          |
| 4 | Rcc-client DEV (main)           | wss://signaling.recoverycat.de                               | Latest stable version: e.g V2 | Recent release is integrated into main and the target url gets updated. |
| 5 | Rcc-client QA (main)            | wss://signaling.recoverycat.de                               | Latest stable version: e.g V2 | E2E tests and manual tests.                                             |
| 6 | Rcc-client Prod (main)          | wss://signaling.recoverycat.de                               | Latest stable version: e.g V2 | Rcc-client gets officially released.                                    |
