# Getting Started

[**_Main Menu_**](../README.md)

1. [Requirements](#requirements)
2. [Running Locally](#running-locally)
3. [Running tests](#running-tests)
4. [Test coverage](#coverage)
5. [Build and Docker Image](#build-docker-file-and-docker-image)

## Requirements

In order to locally run the application, you need the following requirements:

* Java 11 or greater
* SBT 1.9.7

I would recommend to usd SDK for your java and sbt package manager.

## Running locally

Run the following command to start the application locally.

`sbt compile run`

## Running tests

`sbt test`

## Coverage

`sbt clean coverage test coverageReport`

## Generate docker file

`sbt Docker/stage`

## Build docker file and docker image

`sbt Docker/publishLocal`

`docker run --rm -it -p 8080:8080 -p 8081:8081 rcc-websocket-service:0.0.1-SNAPSHOT`

## Formatting

`scalafmtAll`

## Package Dependency

`checkPackageDependencies`
