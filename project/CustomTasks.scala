import sbt.Keys.*
import sbt.{Def, *}

object CustomTasks extends AutoPlugin {

  override lazy val projectSettings: Seq[Def.Setting[?]] = Seq(
    checkPackageDependencies := Def.taskDyn {
      val sourceFiles = (Compile / sources).value
      val log = streams.value.log
      val problemFiles = sourceFiles.filter(file => checkDependencies(file))

      Def.taskIf {
        if (problemFiles.nonEmpty) {
          problemFiles.foreach { file =>
            log.error(s"File ${file.getPath} violates the dependency rule")
          }
          throw new RuntimeException("Dependency rule check failed.")
        } else {
          streams.value.log.success("Dependency rule check passed.")
        }
      }
    }.value
  )

  lazy val checkPackageDependencies = taskKey[Unit]("Check if files violate the dependency rules.")

  private def checkDependencies(file: File): Boolean = {
    val sourceCode = IO.read(file)

    val business = "de.recoverycat.business"
    val application = "de.recoverycat.application"
    val infra = "de.recoverycat.infra"
    val http = "org.http4s"

    val check =
      if (isPackage(sourceCode, business))
        importsPackage(sourceCode, application) || importsPackage(sourceCode, infra) || importsPackage(sourceCode, http)
      else if (isPackage(sourceCode, application))
        importsPackage(sourceCode, infra) || importsPackage(sourceCode, http)
      else
        false

    check
  }

  private def isPackage(sourceCode: String, targetPackage: String): Boolean =
    sourceCode.contains(s"package $targetPackage")

  private def importsPackage(sourceCode: String, targetPackage: String): Boolean =
    sourceCode.contains(s"import $targetPackage")

}
