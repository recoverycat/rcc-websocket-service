import org.typelevel.scalacoptions.*
import scala.Ordering.Implicits.*

val BetterMonadicForVersion = "0.3.1"
val CirceVersion = "0.14.10"
val Http4sPrometheusVersion = "0.25.0"
val Http4sVersion = "0.23.30"
val JansiVersion = "2.4.1"
val JavaWebsocketVersion = "1.6.0"
val kindProjectVersion = "0.13.3"
val LogbackVersion = "1.5.16"
val LogstashVersion = "8.0"
val MunitCatsEffectVersion = "2.0.0"
val MunitVersion = "1.1.0"
val ScalaVersion = "3.3.3"

val buildDocker = taskKey[Unit]("Runs tests and builds docker file and image")

lazy val root = (project in file("."))
  .enablePlugins(DockerPlugin, JavaAppPackaging)
  .enablePlugins(CustomTasks)
  .enablePlugins(UpdatesPlugin)
  .settings(
    organization := "de.recoverycat",
    name := "rcc-websocket-service",
    organizationName := "Recovery Cat GmbH",
    startYear := Some(2023),
    licenses += ("Apache-2.0", new URI("https://www.apache.org/licenses/LICENSE-2.0.txt").toURL),
    version := "0.0.5-SNAPSHOT",
    scalaVersion := ScalaVersion,
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-ember-server" % Http4sVersion,
      "org.http4s" %% "http4s-ember-client" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-prometheus-metrics" % Http4sPrometheusVersion,
      "io.circe" %% "circe-core" % CirceVersion,
      "io.circe" %% "circe-generic" % CirceVersion,
      "io.circe" %% "circe-parser" % CirceVersion,
      "org.scalameta" %% "munit" % MunitVersion % Test,
      "org.typelevel" %% "munit-cats-effect" % MunitCatsEffectVersion % Test,
      "ch.qos.logback" % "logback-classic" % LogbackVersion % Runtime,
      "org.fusesource.jansi" % "jansi" % JansiVersion,
      "net.logstash.logback" % "logstash-logback-encoder" % LogstashVersion,
      "org.java-websocket" % "Java-WebSocket" % JavaWebsocketVersion,
    ),
    libraryDependencies ++= {
      if (scalaVersion.value.startsWith("3.")) Seq.empty
      else
        Seq(
          compilerPlugin(
            ("org.typelevel" %% "kind-projector" % kindProjectVersion).cross(CrossVersion.full)
          ),
          compilerPlugin("com.olegpy" %% "better-monadic-for" % BetterMonadicForVersion),
        )
    },
    assembly / assemblyMergeStrategy := {
      case "module-info.class" => MergeStrategy.discard
      case x => (assembly / assemblyMergeStrategy).value.apply(x)
    },
    Global / onChangedBuildSource := ReloadOnSourceChanges,
    // Docker start
    dockerBaseImage := "azul/zulu-openjdk:17-jre",
    dockerExposedPorts ++= Seq(8080, 8081),
    Docker / daemonUser := "rcc",
    buildDocker := Def.sequential(Test / test, Docker / publishLocal).value,
    // Docker end
    //Scala3 new syntax
    tpolecatScalacOptions ++= Set(ScalacOption("-new-syntax", version => version >= org.typelevel.scalacoptions.ScalaVersion.V3_0_0)),
    //scalacOptions ++= Seq("-new-syntax", "-rewrite"),
    //scalacOptions ++= Seq("-indent", "-rewrite")
  )
