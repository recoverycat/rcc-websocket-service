# Signaling Websocket Service

[![Latest Release](https://git.recoverycat.de/recoverycat/rcc-websocket-service/-/badges/release.svg)](https://git.recoverycat.de/recoverycat/rcc-websocket-service/-/releases)

[![coverage report](https://git.recoverycat.de/recoverycat/rcc-websocket-service/badges/main/coverage.svg)](https://git.recoverycat.de/recoverycat/rcc-websocket-service/-/commits/main)

[![pipeline status](https://git.recoverycat.de/recoverycat/rcc-websocket-service/badges/main/pipeline.svg)](https://git.recoverycat.de/recoverycat/rcc-websocket-service/-/commits/main)

The signaling websocket service is a transmission channel. It supports two main processes:

1. Webrtc signaling.
2. Websocket data transmission.

That is to say that this service allows for two parties to exchange messages in a way that they can synchronize their transmission needs.

1. [Getting Started](docs/a_getting_started.md)
2. [Architecture](docs/b_architecture.md)
3. [Deployment Strategy](docs/c_deployment.md)
4. [Monitoring](docs/d_monitoring.md)

