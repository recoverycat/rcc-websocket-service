/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.util

import cats.effect.{ExitCode, IO}
import com.comcast.ip4s.*
import de.recoverycat.Service
import de.recoverycat.business.ChannelNameValidation

import scala.concurrent.duration.{Duration, DurationInt}
import scala.util.Random

trait ServiceHelper:

  val websocketHost: Host = ipv4"0.0.0.0"
  val websocketPort: Port = port"8080"
  val websocketIdleTimeout: Duration = 60.seconds

  val webAppHost: Host = ipv4"0.0.0.0"
  val webAppPort: Port = port"8081"
  val webAppIdleTimeout: Duration = 60.seconds

  val metricsPrefix: String = "rcc"

  def serviceStream(
      websocketHost: Host = websocketHost,
      websocketPort: Port = websocketPort,
      websocketIdleTimeout: Duration = websocketIdleTimeout,
      webAppHost: Host = webAppHost,
      webAppPort: Port = webAppPort,
      webAppIdleTimeout: Duration = webAppIdleTimeout,
      metricsPrefix: String = metricsPrefix,
  ): IO[ExitCode] = Service.run[IO](
    websocketHost,
    websocketPort,
    websocketIdleTimeout,
    webAppHost,
    webAppPort,
    webAppIdleTimeout,
    metricsPrefix,
  )

  object RandomPortGenerator:

    final val minPort = 8080
    final val maxPort = 65535

    def random = new Random

    def randomPort: Option[Port] = Port.fromInt(minPort + random.nextInt((maxPort - minPort) + 1))

  def websocketUrl(websocketHost: Host, websocketPort: Port): String =
    s"ws://$websocketHost:$websocketPort"

  val waitForStartup: Duration = 1.second
  val waitForMessages: Duration = 500.millis

  val errorJoinChannelIsFull: String =
    """
      |{
      |  "type": "already-in-channel",
      |  "message": "Channel is full",
      |  "isError": true
      |}
      |""".stripMargin

  val errorJoinSameChannelValue: String =
    """
      |{
      | "type": "already-in-channel",
      | "message": "You are already in that channel!",
      | "isError": true
      |}
      |""".stripMargin

  val errorJoinInvalidChannelValue: String =
    s"""
      |{
      | "type": "invalid-input",
      | "message": "Invalid input: Channel name is required and should be ${ChannelNameValidation.minLength} >= length <= ${ChannelNameValidation.maxLength}",
      | "isError": true
      |}
      |""".stripMargin

  val errorInvalidNumberOfPartners: String =
    """
      |{
      |   "type":"invalid-input",
      |   "message":"Invalid input: Invalid number of partners",
      |   "isError":true
      |}
      |""".stripMargin

  val welcomeExpectedValue: String =
    """
      |{
      | "type": "connect",
      | "message": "You are now connected to the Recovery Cat's Signaling Server",
      | "isError": false
      |}
      |""".stripMargin

  def joinValue(channelName: String): String =
    s"""
       |{
       | "type": "join",
       | "channel": "$channelName"
       |}
       |""".stripMargin

  def joinExpectedValue(channelName: String, self: Boolean, count: Int): String =
    s"""
       |{
       |  "type": "joined",
       |  "self": $self,
       |  "channel": "$channelName",
       |  "count": $count,
       |  "message": "listening in $channelName"
       |}
       |""".stripMargin

  def connectedExpectedValue(channelName: String): String =
    s"""
       |{
       |  "type": "joined",
       |  "self": true,
       |  "channel": "$channelName",
       |  "count": 1,
       |  "message": "listening in $channelName"
       |}
       |""".stripMargin
