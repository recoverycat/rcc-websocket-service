/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import cats.data.NonEmptyList
import munit.CatsEffectSuite

class TransferContextsSpec extends CatsEffectSuite:

  trait ContextA extends TransferContext
  trait ContextB extends TransferContext
  trait ContextC extends TransferContext

  test("append should append a new context"):
    val existingContext = new TransferContext {}
    val newContext = new TransferContext {}

    val transferContexts = TransferContexts(Some(NonEmptyList.of(existingContext)))
    val updatedContexts = transferContexts.append(newContext)

    assertEquals(updatedContexts.contexts, Some(NonEmptyList.of(existingContext, newContext)))

  test("get should retrieve a specific type of context"):
    val contextA = new TransferContext with ContextA
    val contextB = new TransferContext with ContextB

    val transferContexts = TransferContexts(Some(NonEmptyList.of(contextA, contextB)))

    val retrievedContextA = transferContexts.get[ContextA]
    assertEquals(retrievedContextA, Some(contextA))

    val retrievedContextB = transferContexts.get[ContextB]
    assertEquals(retrievedContextB, Some(contextB))

  test("get should return None when trying to get a non-existent context"):
    val contextA = new TransferContext with ContextA
    val contextB = new TransferContext with ContextB

    val transferContexts = TransferContexts(Some(NonEmptyList.of(contextA, contextB)))

    val retrievedContextC = transferContexts.get[ContextC]
    assertEquals(retrievedContextC, None)
