/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import munit.CatsEffectSuite

import java.util.UUID

class TransferContextStateSpec extends CatsEffectSuite:

  object ContextA extends TransferContext
  object ContextB extends TransferContext

  test("Append should add TransferContexts to the state"):
    val initialState = TransferContextState.empty
    val requestId = UUID.randomUUID()
    val transferContexts = TransferContexts.of(ContextA)

    val inputControl = InputControl.Append(requestId, transferContexts)

    val (newState, outputControls) = initialState.process(inputControl)

    assert(outputControls.size == 1)
    assert(outputControls.head.isInstanceOf[OutputControl.AppendInfo])

    val infoControl = outputControls.head.asInstanceOf[OutputControl.AppendInfo]
    assert(infoControl.requestId == requestId)
    assert(infoControl.transferContexts == transferContexts)

    val updatedContexts =
      newState.requestIdTransferContexts.getOrElse(requestId, TransferContexts.empty)
    assert(updatedContexts == transferContexts)

  test("Remove should remove TransferContexts from the state"):
    val initialState = TransferContextState.empty
    val requestId = UUID.randomUUID()
    val transferContexts = TransferContexts.of(ContextB)

    val appendControl = InputControl.Append(requestId, transferContexts)
    val (middleState, _) = initialState.process(appendControl)

    val removeControl = InputControl.Remove(requestId, transferContexts)
    val (finalState, outputControls) = middleState.process(removeControl)

    assert(outputControls.size == 1)
    assert(outputControls.head.isInstanceOf[OutputControl.RemoveInfo])

    val infoControl = outputControls.head.asInstanceOf[OutputControl.RemoveInfo]
    assert(infoControl.requestId == requestId)

    assert(finalState.requestIdTransferContexts.isEmpty)
