/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import munit.CatsEffectSuite

import java.util.UUID

class TransferStateSpec extends CatsEffectSuite:

  test("should add join channel"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val channelName = "channel_name"
    val (newTransferState, outputMessages) = transferState.process(JoinChannel(uuid, channelName))

    assertEquals(newTransferState.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> channelName))

    assertEquals(
      outputMessages,
      Seq(
        ChannelJoined(
          Some(uuid),
          Some(uuid),
          self = true,
          channelName,
          1,
          "listening in " + channelName,
        ),
        Signal.ChannelFirstJoinSignal(
          requestId = uuid,
          channel = "channel_name",
        ),
      ),
    )

  test("should transfer data successfully"):
    val transferState = TransferState.empty
    val uuid1 = UUID.randomUUID
    val uuid2 = UUID.randomUUID
    val channelName = "channel_name"
    val data = "5808f5152853c657400284571f5c4ea36d92885f27c7f42084e5fceea9ef0c3e"

    val (newTransferState, outputMessages) = transferState.process(JoinChannel(uuid1, channelName))
    val (newTransferState1, outputMessages1) =
      newTransferState.process(JoinChannel(uuid2, channelName))
    val (newTransferState2, outputMessages2) = newTransferState1.process(TransferData(uuid1, data))

    assertEquals(newTransferState.channelRequestIds, Map(channelName -> Set(uuid1)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid1 -> channelName))

    assertEquals(
      outputMessages,
      Seq(
        ChannelJoined(
          Some(uuid1),
          Some(uuid1),
          self = true,
          channelName,
          1,
          "listening in " + channelName,
        ),
        Signal.ChannelFirstJoinSignal(
          requestId = uuid1,
          channel = "channel_name",
        ),
      ),
    )

    assertEquals(newTransferState1.channelRequestIds, Map(channelName -> Set(uuid1, uuid2)))
    assertEquals(
      newTransferState1.requestIdChannels,
      Map(uuid1 -> channelName, uuid2 -> channelName),
    )

    assertEquals(
      outputMessages1,
      Seq(
        ChannelJoined(
          Some(uuid1),
          Some(uuid2),
          self = false,
          channelName,
          2,
          "listening in " + channelName,
        ),
        ChannelJoined(
          Some(uuid2),
          Some(uuid2),
          self = true,
          channelName,
          2,
          "listening in " + channelName,
        ),
        Signal.ChannelCompletedSignal(Set(uuid1, uuid2), channelName),
      ),
    )

    assertEquals(newTransferState2.channelRequestIds, Map(channelName -> Set(uuid1, uuid2)))
    assertEquals(
      newTransferState2.requestIdChannels,
      Map(uuid1 -> channelName, uuid2 -> channelName),
    )

    assertEquals(
      outputMessages2,
      Seq(
        OutputDataTransferData(Some(uuid2), Some(uuid1), Some(channelName), data),
        Signal.GlobalDataTransferSignal(Set(uuid1, uuid2), channelName),
        Signal.TotalDataTransferSignal(Set(uuid1, uuid2), channelName, 1),
      ),
    )

    assertEquals(newTransferState.effectiveCompleteChannelsCount, 0)
    assertEquals(newTransferState.effectiveChannelsCount, 1)
    assertEquals(newTransferState.tmpChannelsCount, 0)

    assertEquals(newTransferState2.effectiveCompleteChannelsCount, 1)
    assertEquals(newTransferState2.effectiveChannelsCount, 0)
    assertEquals(newTransferState2.tmpChannelsCount, 0)

  test("should count data transfers correctly"):
    val transferState = TransferState.empty
    val uuid1 = UUID.randomUUID
    val uuid2 = UUID.randomUUID
    val channelName = "channel_name"
    val data = "5808f5152853c657400284571f5c4ea36d92885f27c7f42084e5fceea9ef0c3e"

    val (newTransferState, _) = transferState.process(JoinChannel(uuid1, channelName))
    val (newTransferState1, _) =
      newTransferState.process(JoinChannel(uuid2, channelName))
    val (newTransferState2, outputMessages2) = newTransferState1.process(TransferData(uuid1, data))

    assertEquals(
      outputMessages2,
      Seq(
        OutputDataTransferData(Some(uuid2), Some(uuid1), Some(channelName), data),
        Signal.GlobalDataTransferSignal(Set(uuid1, uuid2), channelName),
        Signal.TotalDataTransferSignal(Set(uuid1, uuid2), channelName, 1),
      ),
    )

    val (newTransferState3, outputMessages3) = newTransferState2.process(TransferData(uuid1, data))

    assertEquals(
      outputMessages3,
      Seq(
        OutputDataTransferData(Some(uuid2), Some(uuid1), Some(channelName), data),
        Signal.TotalDataTransferSignal(Set(uuid1, uuid2), channelName, 2),
      ),
    )

    val (_, outputMessages4) = newTransferState3.process(TransferData(uuid2, data))

    assertEquals(
      outputMessages4,
      Seq(
        OutputDataTransferData(Some(uuid1), Some(uuid2), Some(channelName), data),
        Signal.TotalDataTransferSignal(Set(uuid1, uuid2), channelName, 3),
      ),
    )

  test("should get error when trying to join same channel"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val channelName = "channel_name"
    val (newTransferState, _) = transferState.process(JoinChannel(uuid, channelName))
    val (_, outputMessages1) = newTransferState.process(JoinChannel(uuid, channelName))

    assertEquals(newTransferState.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> channelName))

    assertEquals(
      outputMessages1,
      Seq(AlreadyInChannel(Some(uuid), Some(uuid), channelName, "You are already in that channel!")),
    )

  test("should get error when trying to transfer when no other partner"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val channelName = "channel_name"
    val data = "5808f5152853c657400284571f5c4ea36d92885f27c7f42084e5fceea9ef0c3e"

    val (newTransferState, _) = transferState.process(JoinChannel(uuid, channelName))

    val (_, outputMessages1) = newTransferState.process(TransferData(uuid, data))

    assertEquals(newTransferState.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> channelName))

    assertEquals(
      outputMessages1,
      Seq(InvalidInputMessage(Some(uuid), Some(uuid), "Invalid input: Invalid number of partners")),
    )

  test("should disconnect OK - one partner -"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val channelName = "channel_name"

    val (newTransferState, outputMessages) = transferState.process(JoinChannel(uuid, channelName))
    val (newTransferState1, outputMessages1) =
      newTransferState.process(Disconnect(uuid))

    assertEquals(newTransferState.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> channelName))

    assertEquals(
      outputMessages,
      Seq(
        ChannelJoined(
          Some(uuid),
          Some(uuid),
          self = true,
          channelName,
          1,
          "listening in " + channelName,
        ),
        Signal.ChannelFirstJoinSignal(
          requestId = uuid,
          channel = "channel_name",
        ),
      ),
    )

    assertEquals(newTransferState1.channelRequestIds, Map.empty[String, Set[UUID]])
    assertEquals(newTransferState1.requestIdChannels, Map.empty[UUID, String])

    assertEquals(
      outputMessages1,
      Seq(Signal.LeftChannelSignal(uuid, channelName)),
    )

  test("should join tmp channel when not part of any channel"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val defaultChannel = JoinChannel.defaultChannel(uuid)

    val (newTransferState, outputMessages) =
      transferState.process(JoinChannel(uuid, defaultChannel))

    assertEquals(newTransferState.channelRequestIds, Map(defaultChannel -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> defaultChannel))

    assertEquals(
      outputMessages,
      Seq(
        WelcomeMessage(
          to = Some(uuid),
          from = Some(uuid),
        ),
        Signal.DefaultChannelSignal(
          requestId = uuid,
          channel = defaultChannel,
        ),
      ),
    )

  test("should not join tmp channel when already being in a normal channel"):
    val transferState = TransferState.empty
    val uuid = UUID.randomUUID
    val defaultChannel = JoinChannel.defaultChannel(uuid)

    val (newTransferState, outputMessages) =
      transferState.process(JoinChannel(uuid, defaultChannel))

    assertEquals(newTransferState.channelRequestIds, Map(defaultChannel -> Set(uuid)))
    assertEquals(newTransferState.requestIdChannels, Map(uuid -> defaultChannel))

    assertEquals(
      outputMessages,
      Seq(
        WelcomeMessage(
          to = Some(uuid),
          from = Some(uuid),
        ),
        Signal.DefaultChannelSignal(
          requestId = uuid,
          channel = defaultChannel,
        ),
      ),
    )

    val channelName = "channel_name"
    val (newTransferState1, outputMessages1) =
      newTransferState.process(JoinChannel(uuid, channelName))

    assertEquals(newTransferState1.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState1.requestIdChannels, Map(uuid -> channelName))

    assertEquals(
      outputMessages1,
      Seq(
        ChannelJoined(
          Some(uuid),
          Some(uuid),
          self = true,
          channelName,
          1,
          "listening in " + channelName,
        ),
        Signal.ChannelFirstJoinSignal(
          requestId = uuid,
          channel = "channel_name",
        ),
      ),
    )

    val (newTransferState2, outputMessages2) =
      newTransferState1.process(JoinChannel(uuid, defaultChannel))

    assertEquals(newTransferState2.channelRequestIds, Map(channelName -> Set(uuid)))
    assertEquals(newTransferState2.requestIdChannels, Map(uuid -> channelName))

    assertEquals(outputMessages2, Seq.empty)
