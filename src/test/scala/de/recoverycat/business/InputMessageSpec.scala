/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import munit.CatsEffectSuite

import java.util.UUID

class InputMessageSpec extends CatsEffectSuite:

  test("JoinChannel should have correct 'requestId' and 'channel' values"):
    val requestId = UUID.randomUUID()
    val channel = "test_channel"
    val joinChannel = JoinChannel(requestId, channel)

    assertEquals(joinChannel.requestId, requestId)
    assertEquals(joinChannel.channel, channel)

  test(
    "JoinChannel - matchesDefaultChannel should return false for channels not ending with the default suffix"
  ):
    val requestId = UUID.randomUUID()
    val expectedChannel = "test_channel"
    val joinChannel = JoinChannel(requestId, expectedChannel)

    assert(!JoinChannel.matchesDefaultChannel(expectedChannel))
    assert(!JoinChannel.matchesDefaultChannel(requestId, expectedChannel))
    assertEquals(JoinChannel.matchesDefaultChannel(joinChannel), false)

  test(
    "JoinChannel - matchesDefaultChannel should return true for channels ending with the default suffix"
  ):
    val requestId = UUID.randomUUID()
    val defaultChannel = JoinChannel.defaultChannel(requestId)
    val joinChannel = JoinChannel(requestId, defaultChannel)

    assert(JoinChannel.matchesDefaultChannel(defaultChannel))
    assert(JoinChannel.matchesDefaultChannel(requestId, defaultChannel))
    assertEquals(JoinChannel.matchesDefaultChannel(joinChannel), true)

  test(
    "JoinChannel - matchesDefaultChannel should return false for channels not ending with the default suffix"
  ):
    val nonDefaultChannel = "custom_channel"

    assert(!JoinChannel.matchesDefaultChannel(nonDefaultChannel))
    assert(!JoinChannel.matchesDefaultChannel(UUID.randomUUID(), nonDefaultChannel))

  test(
    "JoinChannel - withDefaultChannel should create JoinChannel with a default channel based on the request ID"
  ):
    val requestId = UUID.randomUUID()
    val joinChannel = JoinChannel.withDefaultChannel(requestId)
    val expectedChannel = JoinChannel.defaultChannel(requestId)

    assertEquals(joinChannel, JoinChannel(requestId, expectedChannel))

  test(
    "JoinChannel - withDefaultChannel should create unique default channels for different request IDs"
  ):
    val requestId1 = UUID.randomUUID()
    val requestId2 = UUID.randomUUID()
    val joinChannel1 = JoinChannel.withDefaultChannel(requestId1)
    val joinChannel2 = JoinChannel.withDefaultChannel(requestId2)

    assert(joinChannel1.channel != joinChannel2.channel)

  test("TransferData - should store the provided request ID and data"):
    val requestId = UUID.randomUUID()
    val data = "Test Data"
    val transferData = TransferData(requestId, data)

    assertEquals(transferData.requestId, requestId)
    assertEquals(transferData.data, data)

  test("Disconnect - should store the provided request ID"):
    val requestId = UUID.randomUUID()
    val disconnect = Disconnect(requestId)

    assertEquals(disconnect.requestId, requestId)

  test("InvalidInput - should store the provided request ID and text"):
    val requestId = UUID.randomUUID()
    val text = "Invalid Text"
    val invalidInput = InvalidInput(requestId, text)

    assertEquals(invalidInput.requestId, requestId)
    assertEquals(invalidInput.text, text)
