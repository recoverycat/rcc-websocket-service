/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import munit.CatsEffectSuite

import java.util.UUID

class OutputMessageSpec extends CatsEffectSuite:

  test("WelcomeMessage should have correct 'to' and 'from' values"):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val welcomeMessage = WelcomeMessage(Some(toUUID), Some(fromUUID))

    assertEquals(welcomeMessage.to, Some(toUUID))
    assertEquals(welcomeMessage.from, Some(fromUUID))

  test("LeftChannel should have correct 'to', 'from', and 'message' values"):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val message = "Left channel message"
    val leftChannel = LeftChannel(Some(toUUID), Some(fromUUID), message)

    assertEquals(leftChannel.to, Some(toUUID))
    assertEquals(leftChannel.from, Some(fromUUID))
    assertEquals(leftChannel.message, message)

  test(
    "ChannelJoined should have correct 'to', 'from', 'self', 'channel', 'count', and 'message' values"
  ):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val count = 42
    val message = "Channel joined message"
    val channelJoined =
      ChannelJoined(Some(toUUID), Some(fromUUID), self = true, channel, count, message)

    assertEquals(channelJoined.to, Some(toUUID))
    assertEquals(channelJoined.from, Some(fromUUID))
    assertEquals(channelJoined.self, true)
    assertEquals(channelJoined.channel, channel)
    assertEquals(channelJoined.count, count)
    assertEquals(channelJoined.message, message)

  test("OutputDataTransferData should have correct 'to', 'from', and 'data' values"):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val data = "Test data"
    val outputDataTransferData =
      OutputDataTransferData(Some(toUUID), Some(fromUUID), Some(channel), data)

    assertEquals(outputDataTransferData.to, Some(toUUID))
    assertEquals(outputDataTransferData.from, Some(fromUUID))
    assertEquals(outputDataTransferData.data, data)

  test("KeepAlive should have None 'to' and 'from' values"):
    val keepAlive = KeepAlive

    assertEquals(keepAlive.to, None)
    assertEquals(keepAlive.from, None)

  test("InvalidInputMessage should have correct 'to', 'from', and 'message' values"):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val message = "Invalid input message"
    val invalidInputMessage = InvalidInputMessage(Some(toUUID), Some(fromUUID), message)

    assertEquals(invalidInputMessage.to, Some(toUUID))
    assertEquals(invalidInputMessage.from, Some(fromUUID))
    assertEquals(invalidInputMessage.message, message)

  test("AlreadyInChannel should have correct 'to', 'from', and 'message' values"):
    val toUUID = UUID.randomUUID()
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val message = "Already in channel message"
    val alreadyInChannel = AlreadyInChannel(Some(toUUID), Some(fromUUID), channel, message)

    assertEquals(alreadyInChannel.to, Some(toUUID))
    assertEquals(alreadyInChannel.from, Some(fromUUID))
    assertEquals(alreadyInChannel.channel, channel)
    assertEquals(alreadyInChannel.message, message)

  test("LeftChannelSignal should have correct 'from', 'channel' values"):
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val leftChannelSignal = Signal.LeftChannelSignal(fromUUID, channel)

    assertEquals(leftChannelSignal.requestId, fromUUID)
    assertEquals(leftChannelSignal.channel, channel)

  test("WelcomeMessage should return true for the specified request ID in 'forRequestId'"):
    val targetRequestId = UUID.randomUUID()
    val welcomeMessage = WelcomeMessage(Some(targetRequestId))

    assertEquals(welcomeMessage.forRequestId(targetRequestId), true)
    assertEquals(welcomeMessage.forRequestId(UUID.randomUUID()), false)

  test("Signal.LeftChannelSignal should have correct 'from', 'channel' values"):
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val leftChannelSignal = Signal.LeftChannelSignal(fromUUID, channel)

    assertEquals(leftChannelSignal.requestId, fromUUID)
    assertEquals(leftChannelSignal.channel, channel)

  test("Signal.DefaultChannelSignal should have correct 'from', 'channel' values"):
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val defaultChannelSignal = Signal.DefaultChannelSignal(fromUUID, channel)

    assertEquals(defaultChannelSignal.requestId, fromUUID)
    assertEquals(defaultChannelSignal.channel, channel)

  test("Signal.ChannelFirstJoinSignal should have correct 'from', 'channel' values"):
    val fromUUID = UUID.randomUUID()
    val channel = "test_channel"
    val channelFirstJoinSignal = Signal.ChannelFirstJoinSignal(fromUUID, channel)

    assertEquals(channelFirstJoinSignal.requestId, fromUUID)
    assertEquals(channelFirstJoinSignal.channel, channel)

  test("Signal.ChannelCompletedSignal should have correct 'channel' and 'requestIds' values"):
    val channel = "test_channel"
    val requestIds = Set(UUID.randomUUID(), UUID.randomUUID())
    val channelCompletedSignal = Signal.ChannelCompletedSignal(requestIds, channel)

    assertEquals(channelCompletedSignal.requestIds, requestIds)
    assertEquals(channelCompletedSignal.channel, channel)
