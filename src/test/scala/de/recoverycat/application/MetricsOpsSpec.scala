/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.effect.IO
import munit.CatsEffectSuite

class MetricsOpsSpec extends CatsEffectSuite:

  // Mock implementation of MetricsOps using IO
  class MockMetricsOps extends MetricsOps[IO]:
    var channelsResult: Option[(Int, Option[String])] = None
    var sessionDurationResult: Option[(Long, Option[String])] = None

    override def setChannels(channels: Int, classifier: Option[String]): IO[Unit] =
      IO { channelsResult = Some((channels, classifier)) }

    override def observeSessionDuration(elapsed: Long, classifier: Option[String]): IO[Unit] =
      IO { sessionDurationResult = Some((elapsed, classifier)) }

  test("setChannels should set channels and classifier"):
    val mockMetricsOps = new MockMetricsOps
    val channels = 5
    val classifier = Some("some_classifier")

    mockMetricsOps.setChannels(channels, classifier).unsafeRunSync()

    assertEquals(mockMetricsOps.channelsResult, Some((channels, classifier)))

  test("observeSessionDuration should set elapsed time and classifier"):
    val mockMetricsOps = new MockMetricsOps
    val elapsed = 1000L
    val classifier = Some("some_classifier")

    mockMetricsOps.observeSessionDuration(elapsed, classifier).unsafeRunSync()

    assertEquals(mockMetricsOps.sessionDurationResult, Some((elapsed, classifier)))
