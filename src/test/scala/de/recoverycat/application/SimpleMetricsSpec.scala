/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.data.NonEmptyList
import cats.effect.IO
import de.recoverycat.application.SimpleMetrics.{ChannelDataTransfers, ChannelRequests, RequestInfo}
import de.recoverycat.business.{
  TransferContext,
  TransferContextState,
  TransferContexts,
  TransferState,
}
import munit.CatsEffectSuite

import java.util.UUID
import scala.concurrent.duration.Duration

class SimpleMetricsSpec extends CatsEffectSuite:

  test("should have same values with empty"):

    val transferState = TransferState.empty
    val transferContextState = TransferContextState.empty
    val simpleMetrics = SimpleMetrics.impl[IO]

    val channelRequests = Nil
    val requestInfo = Nil
    val channelDataTransfers = Nil
    val resp = SimpleMetrics.Metrics(
      transferState.effectiveCompleteChannelsCount,
      transferState.effectiveChannelsCount,
      transferState.tmpChannelsCount,
      channelRequests,
      requestInfo,
      channelDataTransfers,
    )

    assertIO(simpleMetrics.build(transferState, transferContextState), resp)

  test("should have same values when non-empty"):

    val uuid1 = UUID.randomUUID()
    val channel = "channel_name"

    val transferState = TransferState(
      requestIdChannels = Map(uuid1 -> channel),
      channelRequestIds = Map(channel -> Set(uuid1)),
      channelDataTransfer = Map(channel -> 100),
    )

    val startTime = RequestTransferContext.StartTime(Duration.fromNanos(System.nanoTime))
    val endTime = RequestTransferContext.EndTime(Duration.fromNanos(System.nanoTime))
    val origin = RequestTransferContext.Origin(
      Some(NonEmptyList.one("Origin"))
    )
    val remoteAddress = RequestTransferContext.RemoteAddress(
      Some(NonEmptyList.one("Remote-Address"))
    )
    val userAgent = RequestTransferContext.UserAgent(
      Some(NonEmptyList.one("User-Agent"))
    )

    val transferContexts = TransferContexts(
      Some(NonEmptyList.of(startTime, origin, remoteAddress, userAgent, endTime))
    )

    val transferContextState = TransferContextState(Map(uuid1 -> transferContexts))
    val simpleMetrics = SimpleMetrics.impl[IO]

    val channelRequests = List(
      ChannelRequests(
        channel,
        Set(uuid1),
        1,
      )
    )
    val requestInfo = List(
      RequestInfo(
        requestId = uuid1,
        startTime = Option(startTime.value.toNanos),
        origin = Option("Origin"),
        remoteAddress = Option("Remote-Address"),
        userAgent = Option("User-Agent"),
      )
    )

    val channelDataTransfers = List(
      ChannelDataTransfers(
        channelName = "channel_name",
        currentCount = 100,
      )
    )

    val resp = SimpleMetrics.Metrics(
      transferState.effectiveCompleteChannelsCount,
      transferState.effectiveChannelsCount,
      transferState.tmpChannelsCount,
      channelRequests,
      requestInfo,
      channelDataTransfers,
    )

    assertIO(simpleMetrics.build(transferState, transferContextState), resp)

  test("should have same values when non-empty with other context"):

    val uuid1 = UUID.randomUUID()
    val channel = "channel_name"

    val transferState = TransferState(
      requestIdChannels = Map(uuid1 -> channel),
      channelRequestIds = Map(channel -> Set(uuid1)),
      channelDataTransfer = Map(channel -> 0),
    )

    val startTime = RequestTransferContext.StartTime(Duration.fromNanos(System.nanoTime))
    val endTime = RequestTransferContext.EndTime(Duration.fromNanos(System.nanoTime))
    val origin = RequestTransferContext.Origin(
      Some(NonEmptyList.one("Origin"))
    )
    val remoteAddress = RequestTransferContext.RemoteAddress(
      Some(NonEmptyList.one("Remote-Address"))
    )
    val userAgent = RequestTransferContext.UserAgent(
      Some(NonEmptyList.one("User-Agent"))
    )

    // Adding other context as simple test
    object ContextA extends TransferContext

    val transferContexts = TransferContexts(
      Some(NonEmptyList.of(startTime, origin, remoteAddress, userAgent, endTime, ContextA))
    )

    val transferContextState = TransferContextState(Map(uuid1 -> transferContexts))
    val simpleMetrics = SimpleMetrics.impl[IO]

    val channelRequests = List(
      ChannelRequests(
        channel,
        Set(uuid1),
        1,
      )
    )
    val requestInfo = List(
      RequestInfo(
        requestId = uuid1,
        startTime = Option(startTime.value.toNanos),
        origin = Option("Origin"),
        remoteAddress = Option("Remote-Address"),
        userAgent = Option("User-Agent"),
      )
    )

    val channelDataTransfers = List(
      ChannelDataTransfers(
        channelName = "channel_name",
        currentCount = 0,
      )
    )

    val resp = SimpleMetrics.Metrics(
      transferState.effectiveCompleteChannelsCount,
      transferState.effectiveChannelsCount,
      transferState.tmpChannelsCount,
      channelRequests,
      requestInfo,
      channelDataTransfers,
    )

    assertIO(simpleMetrics.build(transferState, transferContextState), resp)
