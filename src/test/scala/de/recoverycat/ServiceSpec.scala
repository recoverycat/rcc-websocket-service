/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat

import cats.effect.IO
import cats.effect.std.Dispatcher
import com.comcast.ip4s.*
import de.recoverycat.util.{ServiceHelper, WebsocketClientHelper}
import io.circe.parser.*
import munit.CatsEffectSuite

import java.net.URI
import scala.io.Source

class ServiceSpec extends CatsEffectSuite with ServiceHelper with WebsocketClientHelper:

  test("should receive welcome message"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msg <- client.messages.take

        _ <- client.close
      yield
        assertEquals(size, 1)
        assertEquals(msg, parse(welcomeExpectedValue).toTry.get.noSpaces)
    }

  test("should get connected message after sending join message"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- client.send(joinValue(channelName))
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msgs <- client.messages.tryTakeN(Some(size))

        _ <- client.close
      yield msgs match
        case Nil => fail("no messages read")
        case List(head, next) =>
          assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
          assertEquals(next, parse(connectedExpectedValue(channelName)).toTry.get.noSpaces)
        case List(head) =>
          assertEquals(head, parse(connectedExpectedValue(channelName)).toTry.get.noSpaces)
        case _ => fail("unexpected values")
    }

  test("should get error getting connected on invalid channel"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = ""

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- IO.sleep(waitForMessages)
        _ <- client.send(joinValue(channelName))
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msgs <- client.messages.tryTakeN(Some(size))

        _ <- client.close
      yield msgs match
        case Nil => fail("no messages read")
        case List(head, next) =>
          assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
          assertEquals(next, parse(errorJoinInvalidChannelValue).toTry.get.noSpaces)
        case List(head) =>
          assertEquals(head, parse(errorJoinInvalidChannelValue).toTry.get.noSpaces)
        case _ => fail("unexpected values")
    }

  test("should get error message after joining with the same channel"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- client.send(joinValue(channelName))
        _ <- client.send(joinValue(channelName))
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msgs <- client.messages.tryTakeN(Some(size))

        _ <- client.close
      yield msgs match
        case Nil => fail("no messages read")
        case List(head, next, tail) =>
          assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
          assertEquals(
            next,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(tail, parse(errorJoinSameChannelValue).toTry.get.noSpaces)
        case List(head, tail) =>
          assertEquals(
            head,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(tail, parse(errorJoinSameChannelValue).toTry.get.noSpaces)
        case x => fail(s"unexpected values ${x.mkString(", ")}")
    }

  test("should be able to change channel"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"

    val newChannelName = "other_channel_name"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- client.send(joinValue(channelName))
        _ <- client.send(joinValue(newChannelName))
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msgs <- client.messages.tryTakeN(Some(size))

        _ <- client.close
      yield msgs match
        case Nil => fail("no messages read")
        case List(head, next, tail) =>
          assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
          assertEquals(
            next,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(
            tail,
            parse(joinExpectedValue(newChannelName, self = true, count = 1)).toTry.get.noSpaces,
          )
        case List(head, tail) =>
          assertEquals(
            head,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(
            tail,
            parse(joinExpectedValue(newChannelName, self = true, count = 1)).toTry.get.noSpaces,
          )
        case other => fail(s"unexpected values ${other.mkString(", ")}")
    }

  test("should get error message trying to transfer data with no other connected client"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"
    val data = "5808f5152853c657400284571f5c4ea36d92885f27c7f42084e5fceea9ef0c3e"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client.connect
        _ <- client.send(joinValue(channelName))
        _ <- client.send(data)
        _ <- IO.sleep(waitForMessages)
        size <- client.messages.size
        msgs <- client.messages.tryTakeN(Some(size))

        _ <- client.close
      yield msgs match
        case Nil => fail("no messages read")
        case List(head, next, tail) =>
          assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
          assertEquals(
            next,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(tail, parse(errorInvalidNumberOfPartners).toTry.get.noSpaces)
        case List(head, tail) =>
          assertEquals(
            head,
            parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
          )
          assertEquals(tail, parse(errorInvalidNumberOfPartners).toTry.get.noSpaces)
        case x => fail(s"unexpected values ${x.mkString(", ")}")
    }

  test("should transfer data successfully"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"
    val data = "5808f5152853c657400284571f5c4ea36d92885f27c7f42084e5fceea9ef0c3e"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client1 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client1.connect
        _ <- client1.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        client2 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client2.connect
        _ <- client2.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        _ <- client1.send(data)
        _ <- IO.sleep(waitForMessages)

        sizeFromClient1 <- client1.messages.size
        msgsFromClient1 <- client1.messages.tryTakeN(Some(sizeFromClient1))

        sizeFromClient2 <- client2.messages.size
        msgsFromClient2 <- client2.messages.tryTakeN(Some(sizeFromClient2))
        _ <- IO.sleep(waitForMessages)

        _ <- client1.close
        _ <- client2.close
      yield

        msgsFromClient1 match
          case Nil => fail("no messages read")
          case List(head, next, tail) =>
            assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
            assertEquals(
              next,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(
              tail,
              parse(joinExpectedValue(channelName, self = false, count = 2)).toTry.get.noSpaces,
            )
          case List(head, tail) =>
            assertEquals(
              head,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(
              tail,
              parse(joinExpectedValue(channelName, self = false, count = 2)).toTry.get.noSpaces,
            )
          case x => fail(s"unexpected values \n${x.mkString("\n")}")

        msgsFromClient2 match
          case Nil => fail("no messages read")
          case List(head, next, tail) =>
            assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
            assertEquals(
              next,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(tail, parse(errorInvalidNumberOfPartners).toTry.get.noSpaces)
          case List(head, tail) =>
            assertEquals(
              head,
              parse(joinExpectedValue(channelName, self = true, count = 2)).toTry.get.noSpaces,
            )
            assertEquals(tail, data)
          case x => fail(s"unexpected values \n${x.mkString("\n")}")
    }

  test("should transfer big amounts of data successfully"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"
    val data = Source.fromResource("fixture-fake-data").getLines().mkString(" ")

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client1 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client1.connect
        _ <- client1.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        client2 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client2.connect
        _ <- client2.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        _ <- client1.send(data)
        _ <- IO.sleep(waitForMessages)

        sizeFromClient1 <- client1.messages.size
        msgsFromClient1 <- client1.messages.tryTakeN(Some(sizeFromClient1))

        sizeFromClient2 <- client2.messages.size
        msgsFromClient2 <- client2.messages.tryTakeN(Some(sizeFromClient2))
        _ <- IO.sleep(waitForMessages)

        _ <- client1.close
        _ <- client2.close
      yield

        msgsFromClient1 match
          case Nil => fail("no messages read")
          case List(head, next, tail) =>
            assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
            assertEquals(
              next,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(
              tail,
              parse(joinExpectedValue(channelName, self = false, count = 2)).toTry.get.noSpaces,
            )
          case List(head, tail) =>
            assertEquals(
              head,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(
              tail,
              parse(joinExpectedValue(channelName, self = false, count = 2)).toTry.get.noSpaces,
            )
          case x => fail(s"unexpected values \n${x.mkString("\n")}")

        msgsFromClient2 match
          case Nil => fail("no messages read")
          case List(head, next, tail) =>
            assertEquals(head, parse(welcomeExpectedValue).toTry.get.noSpaces)
            assertEquals(
              next,
              parse(joinExpectedValue(channelName, self = true, count = 1)).toTry.get.noSpaces,
            )
            assertEquals(tail, parse(errorInvalidNumberOfPartners).toTry.get.noSpaces)
          case List(head, tail) =>
            assertEquals(
              head,
              parse(joinExpectedValue(channelName, self = true, count = 2)).toTry.get.noSpaces,
            )
            assertEquals(tail, data)
          case x => fail(s"unexpected values \n${x.mkString("\n")}")
    }

  test("should only allow two connections to an existing channel"):

    val websocketPort = RandomPortGenerator.randomPort.getOrElse(port"8080")
    val webAppPort = RandomPortGenerator.randomPort.getOrElse(port"8081")

    val channelName = "channel_name"

    Dispatcher.parallel[IO].use { dispatcher =>
      for
        _ <- IO.pure(
          dispatcher.unsafeRunAndForget(
            serviceStream(websocketPort = websocketPort, webAppPort = webAppPort)
          )
        )
        _ <- IO.sleep(waitForStartup)

        client1 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client1.connect
        _ <- client1.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        client2 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client2.connect
        _ <- client2.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        client3 <- createClient(
          URI.create(websocketUrl(websocketHost, websocketPort)),
          dispatcher,
        )
        _ <- client3.connect
        _ <- client3.send(joinValue(channelName))

        _ <- IO.sleep(waitForMessages)

        sizeFromClient1 <- client1.messages.size
        msgsFromClient1 <- client1.messages.tryTakeN(Some(sizeFromClient1))

        sizeFromClient2 <- client2.messages.size
        msgsFromClient2 <- client2.messages.tryTakeN(Some(sizeFromClient2))

        sizeFromClient3 <- client3.messages.size
        msgsFromClient3 <- client3.messages.tryTakeN(Some(sizeFromClient3))

        _ <- client1.close
        _ <- client2.close
        _ <- client3.close
      yield
        assert(
          msgsFromClient1.contains(
            parse(joinExpectedValue(channelName, self = false, count = 2)).toTry.get.noSpaces
          )
        )
        assert(
          msgsFromClient2.contains(
            parse(joinExpectedValue(channelName, self = true, count = 2)).toTry.get.noSpaces
          )
        )
        assert(msgsFromClient3.contains(parse(errorJoinChannelIsFull).toTry.get.noSpaces))
    }
