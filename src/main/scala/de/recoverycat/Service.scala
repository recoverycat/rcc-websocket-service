/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat

import cats.effect.kernel.Ref
import cats.effect.std.Queue
import cats.effect.{Async, ExitCode, Resource}
import cats.implicits.*
import com.comcast.ip4s.*
import de.recoverycat.application.view.TransferStateView
import de.recoverycat.application.{SimpleMetrics, Transfer, TransferStateReviewer}
import de.recoverycat.business.{
  InputMessage,
  OutputControl,
  OutputMessage,
  TransferContextState,
  TransferState,
}
import de.recoverycat.infra.prometheus.PrometheusMetrics
import de.recoverycat.infra.web.{
  InputParser,
  OutputParser,
  PrometheusMiddleware,
  Routes,
  TransferMessageAdaptor,
  WebStreams,
}
import fs2.*
import fs2.concurrent.Topic
import fs2.io.net.Network
import org.http4s.HttpApp
import org.http4s.implicits.*
import org.http4s.metrics.prometheus.PrometheusExportService
import org.http4s.server.websocket.WebSocketBuilder2
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import scala.concurrent.duration.Duration

object Service:

  def run[F[_]: Async: Network](
      websocketHost: Host,
      websocketPort: Port,
      websocketIdleTimeout: Duration,
      webAppHost: Host,
      webAppPort: Port,
      webAppIdleTimeout: Duration,
      metricsPrefix: String,
  ): F[ExitCode] = {
    for

      logger: SelfAwareStructuredLogger[F] <- Stream.eval(Slf4jLogger.create[F])

      metricsSvc <- Stream.resource(PrometheusExportService.build[F])
      metricsOps <- Stream.resource(
        PrometheusMetrics.metricsOps(metricsSvc.collectorRegistry, metricsPrefix)
      )
      simpleMetrics = SimpleMetrics.impl[F]

      inputQueue <- Stream.eval(Queue.unbounded[F, InputMessage])
      outputTopic <- Stream.eval(Topic[F, OutputMessage])
      viewQueue <- Stream.eval(Queue.unbounded[F, OutputMessage])
      transferContextQueue <- Stream.eval(Queue.unbounded[F, OutputControl])

      transferState <- Stream.eval(Ref.of[F, TransferState](TransferState.empty))
      transferContextState <- Stream.eval(
        Ref.of[F, TransferContextState](TransferContextState.empty)
      )

      iParser = InputParser.impl
      oParser = OutputParser.impl
      transferAlg = Transfer.impl[F](
        outputTopic,
        inputQueue,
        viewQueue,
        transferContextQueue,
        transferState,
        transferContextState,
        iParser,
        oParser,
      )

      transferView = TransferStateView.impl[F](
        viewQueue,
        transferContextQueue,
        transferState,
        transferContextState,
        metricsOps,
        logger,
      )

      transferReviewer = TransferStateReviewer.impl[F](transferState, transferContextState)

      exitCode <-

        val processingStream = transferAlg.process

        val processingStreamView = transferView.process

        val processingStreamReviewer = transferReviewer.process

        val adaptor = TransferMessageAdaptor.impl

        val webSocketWebApp: WebSocketBuilder2[F] => HttpApp[F] =
          ws => Routes.websocketRoutes[F](ws, transferAlg, adaptor).orNotFound
        val websocketStream = Stream.resource(
          WebStreams.websocketResource(websocketHost, websocketPort, websocketIdleTimeout)(
            webSocketWebApp
          ) >> Resource.never
        )

        val webApp = Routes.simpleMetrics[F](simpleMetrics, transferState, transferContextState)
        val webStream =
          for
            meteredApp <- Stream.resource(
              PrometheusMiddleware.meteredRouter[F](metricsSvc, metricsPrefix, webApp)
            )
            res <- Stream.resource(
              WebStreams.webResource(webAppHost, webAppPort, webAppIdleTimeout)(
                meteredApp.orNotFound
              ) >> Resource.never
            )
          yield res

        Stream(
          webStream,
          websocketStream,
          processingStream,
          processingStreamView,
          processingStreamReviewer,
        ).parJoinUnbounded
    yield exitCode

  }.compile.drain.as(ExitCode.Success)
