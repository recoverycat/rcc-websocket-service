/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application.view

import cats.effect.Async
import cats.implicits.*
import de.recoverycat.application.{MetricsOps, RequestTransferContext}
import de.recoverycat.business.OutputControl

private[view] trait ViewDuration[F[_]]:

  def observeSessionDuration(
      outputControl: OutputControl,
      classifier: Option[String],
  ): F[Unit]

private[view] object ViewDuration:
  def impl[F[_]: Async](metricsOps: MetricsOps[F]): ViewDuration[F] =
    new ViewDuration[F]:

      def observeSessionDuration(
          outputControl: OutputControl,
          classifier: Option[String],
      ): F[Unit] =
        for res <-
            outputControl match
              case OutputControl.AppendInfo(_, _) => ().pure[F]
              case OutputControl.RemoveInfo(_, transferContexts) =>
                RequestTransferContext
                  .calculateElapsedTime(transferContexts)
                  .map(_.toNanos)
                  .map { elapsedAsNanos =>
                    metricsOps.observeSessionDuration(elapsedAsNanos, classifier)
                  }
                  .getOrElse(().pure[F])
        yield res
