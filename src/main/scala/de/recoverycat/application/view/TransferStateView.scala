/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application.view

import cats.effect.Async
import cats.effect.kernel.Ref
import cats.effect.std.Queue
import de.recoverycat.application.MetricsOps
import de.recoverycat.business.{OutputControl, OutputMessage, TransferContextState, TransferState}
import fs2.Stream
import org.typelevel.log4cats.SelfAwareStructuredLogger

trait TransferStateView[F[_]]:

  def process: Stream[F, Nothing]

object TransferStateView:

  def impl[F[_]: Async](
      viewQueue: Queue[F, OutputMessage],
      transferContextQueue: Queue[F, OutputControl],
      transferStateRef: Ref[F, TransferState],
      transferContextStateRef: Ref[F, TransferContextState],
      metricsOps: MetricsOps[F],
      logger: SelfAwareStructuredLogger[F],
  ): TransferStateView[F] = new TransferStateView[F]:

    private val viewRegistry =
      ViewRegistry.impl(transferStateRef, transferContextStateRef, metricsOps, logger)

    override def process: Stream[F, Nothing] =

      val viewQueueStream = Stream
        .fromQueueUnterminated(viewQueue)
        .flatMap { outputMessage =>
          Stream.eval(
            viewRegistry.logOutputMessage(outputMessage)
          ) >> Stream.exec(viewRegistry.setMetrics(None))
        }

      val transferContextQueueStream =
        Stream
          .fromQueueUnterminated(transferContextQueue)
          .flatMap { outputControl =>
            Stream.eval(viewRegistry.logOutputControl(outputControl)) >> Stream.exec(
              viewRegistry.observeSessionDuration(outputControl, None)
            )
          }

      Stream(viewQueueStream, transferContextQueueStream).parJoinUnbounded
