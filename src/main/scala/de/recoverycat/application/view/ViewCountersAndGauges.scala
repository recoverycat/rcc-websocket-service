/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application.view

import cats.effect.Async
import cats.effect.kernel.Ref
import cats.implicits.*
import de.recoverycat.application.MetricsOps
import de.recoverycat.business.TransferState

private[view] trait ViewCountersAndGauges[F[_]]:

  def setMetrics(classifier: Option[String]): F[Unit]

private[view] object ViewCountersAndGauges:
  def impl[F[_]](
      transferStateRef: Ref[F, TransferState],
      metricsOps: MetricsOps[F],
  )(using F: Async[F]): ViewCountersAndGauges[F] =
    new ViewCountersAndGauges[F]:

      private final val EFFECTIVE_COMPLETE = Some("effective_complete")
      private final val EFFECTIVE = Some("effective")
      private final val TMP = Some("tmp")

      def setMetrics(classifier: Option[String]): F[Unit] =
        for
          transferState <- transferStateRef.get

          _ <- metricsOps.setChannels(
            transferState.effectiveCompleteChannelsCount,
            EFFECTIVE_COMPLETE,
          )

          _ <- metricsOps.setChannels(transferState.effectiveChannelsCount, EFFECTIVE)
          res <- metricsOps.setChannels(transferState.tmpChannelsCount, TMP)
        yield res
