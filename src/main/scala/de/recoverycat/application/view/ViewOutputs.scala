/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application.view

import cats.effect.Async
import cats.effect.kernel.Ref
import cats.implicits.*
import de.recoverycat.application.RequestTransferContext
import de.recoverycat.business.Signal.ChannelCompletedSignal
import de.recoverycat.business.{
  AlreadyInChannel,
  InvalidInputMessage,
  OutputControl,
  OutputMessage,
  OutputMessageTargeted,
  Signal,
  TransferContextState,
}
import fs2.Stream
import org.typelevel.log4cats.SelfAwareStructuredLogger

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.time.LocalDate
import java.util.{Base64, UUID}

private[view] trait ViewOutputs[F[_]]:

  def logOutputMessage(outputMessage: OutputMessage): F[Unit]
  def logOutputControl(outputControl: OutputControl): F[Unit]

private[view] object ViewOutputs:
  def impl[F[_]](
      transferContextStateRef: Ref[F, TransferContextState],
      logger: SelfAwareStructuredLogger[F],
  )(using F: Async[F]): ViewOutputs[F] = new ViewOutputs[F]:

    private final val SL_NAMESPACE = "rcc.wss."
    private final val SL_ORIGIN_KEY = SL_NAMESPACE + "origin_"
    private final val SL_CHANNEL_KEY = SL_NAMESPACE + "channel"
    private final val SL_REMOTE_ADDRESS_KEY = SL_NAMESPACE + "remote_address_"
    private final val SL_USER_AGENT_KEY = SL_NAMESPACE + "user_agent_"
    private final val SL_ANONYMIZED_FINGERPRINT = SL_NAMESPACE + "anonymized_fingerprint_"

    def logOutputMessage(outputMessage: OutputMessage): F[Unit] = F.defer:

      val log1 = outputMessage match
        case targeted: OutputMessageTargeted =>
          targeted match
            case invalidInput @ InvalidInputMessage(_, _, _) =>
              logger.error(invalidInput.toString)
            case alreadyInChannel @ AlreadyInChannel(_, _, _, _) =>
              logger.warn(alreadyInChannel.toString)
            case other =>
              logger.debug(other.toString)
        case _: Signal => ().pure[F]

      val log2 = logOutputMessageAsSL(outputMessage)

      log1 >> log2

    def logOutputControl(outputControl: OutputControl): F[Unit] =
      logger.debug(outputControl.toString)

    def logOutputMessageAsSL(outputMessage: OutputMessage): F[Unit] = F.defer:

      def doLog(requestIds: Set[UUID], channel: String, outputMessage: OutputMessage): F[Unit] =
        for
          transferContextState <- transferContextStateRef.get
          ctx <- getCtx(transferContextState, requestIds, channel)
          res <- logger.info(ctx)(outputMessage.toString)
        yield res

      outputMessage match
        case targeted: OutputMessageTargeted =>
          targeted match
            case _ => ().pure[F]
        case signal: Signal =>
          signal match
            case Signal.DefaultChannelSignal(requestId, channel) =>
              doLog(Set(requestId), channel, signal)
            case Signal.ChannelFirstJoinSignal(requestId, channel) =>
              doLog(Set(requestId), channel, signal)
            case ChannelCompletedSignal(requestIds, channel) =>
              doLog(requestIds, channel, signal)
            case Signal.GlobalDataTransferSignal(requestIds, channel) =>
              doLog(requestIds, channel, signal)
            case Signal.TotalDataTransferSignal(requestIds, channel, _) =>
              doLog(requestIds, channel, signal)
            case Signal.LeftChannelSignal(requestId, channel) =>
              doLog(Set(requestId), channel, signal)

    private def getCtx(
        transferContextState: TransferContextState,
        requestIds: Set[UUID],
        channel: String,
    ): F[Map[String, String]] =
      for
        originMap <- getOriginMap(transferContextState, requestIds)
        userAgentMap <- getUserAgentMap(transferContextState, requestIds)
        channelMap <- getChannelMap(channel)
        anonymizedFingerprint <- getAnonymizedFingerprint(transferContextState, requestIds)
      yield (
        originMap ++ userAgentMap ++ anonymizedFingerprint
      ).foldLeft(channelMap) { case (a, b) => a ++ b }

    private def getChannelMap(channel: String): F[Map[String, String]] =
      F.delay:
        Map(
          SL_CHANNEL_KEY -> channel
        )

    private def getRemoteAddressMap(
        transferContextState: TransferContextState,
        requestIds: Set[UUID],
    ): F[List[Map[String, String]]] =
      Stream
        .iterable(requestIds)
        .zipWithIndex
        .flatMap { case (uuid, i) =>
          Stream.eval:
            F.delay:
              transferContextState.requestIdTransferContexts
                .get(uuid)
                .flatMap(_.get[RequestTransferContext.RemoteAddress])
                .collect { case RequestTransferContext.RemoteAddress(Some(value)) =>
                  Map(SL_REMOTE_ADDRESS_KEY + i -> value.head)
                }
                .getOrElse(Map.empty)
        }
        .filter(_.nonEmpty)
        .covary[F]
        .compile
        .toList

    private def getUserAgentMap(
        transferContextState: TransferContextState,
        requestIds: Set[UUID],
    ): F[List[Map[String, String]]] =
      Stream
        .iterable(requestIds)
        .zipWithIndex
        .flatMap { case (uuid, i) =>
          Stream.eval:
            F.delay:
              transferContextState.requestIdTransferContexts
                .get(uuid)
                .flatMap(_.get[RequestTransferContext.UserAgent])
                .collect { case RequestTransferContext.UserAgent(Some(value)) =>
                  Map(SL_USER_AGENT_KEY + i -> value.head)
                }
                .getOrElse(Map.empty)
        }
        .filter(_.nonEmpty)
        .covary[F]
        .compile
        .toList

    private def getOriginMap(
        transferContextState: TransferContextState,
        requestIds: Set[UUID],
    ): F[List[Map[String, String]]] =
      Stream
        .iterable(requestIds)
        .zipWithIndex
        .flatMap { case (uuid, i) =>
          Stream.eval:
            F.delay:
              transferContextState.requestIdTransferContexts
                .get(uuid)
                .flatMap(_.get[RequestTransferContext.Origin])
                .collect { case RequestTransferContext.Origin(Some(value)) =>
                  Map(SL_ORIGIN_KEY + i -> value.head)
                }
                .getOrElse(Map.empty)
        }
        .filter(_.nonEmpty)
        .covary[F]
        .compile
        .toList

    private def getAnonymizedFingerprint(
        transferContextState: TransferContextState,
        requestIds: Set[UUID],
    ): F[List[Map[String, String]]] =

      def getHash(data: String): F[String] =
        F.delay(MessageDigest.getInstance("SHA-256"))
          .map(_.digest(data.getBytes(StandardCharsets.UTF_8)))
          .map(_.take(8))
          .map(bytes => Base64.getUrlEncoder.encodeToString(bytes))

      def getStringData(data: List[Map[String, String]]): F[String] =
        F.delay:
          data
            .foldRight(Map("DATUM" -> LocalDate.now().toString)) { case (a, b) => a ++ b }
            .values
            .mkString(".")

      def mergeMaps(map1: Map[String, String], map2: Map[String, String]): Map[String, String] =
        (map1.toSeq ++ map2.toSeq)
          .groupBy(_._1)
          .view
          .mapValues(_.map(_._2).mkString)
          .toMap

      def getMerged(uuid: UUID) =
        def mergeListsOfMaps(
            list1: List[Map[String, String]],
            list2: List[Map[String, String]],
        ): List[Map[String, String]] =
          list1.zipAll(list2, Map.empty, Map.empty).map { case (map1, map2) =>
            mergeMaps(map1, map2)
          }
        for
          remoteAddressMap <- getRemoteAddressMap(transferContextState, Set(uuid))
          userAgentMap <- getUserAgentMap(transferContextState, Set(uuid))
        yield
          if remoteAddressMap.nonEmpty then mergeListsOfMaps(remoteAddressMap, userAgentMap)
          else List.empty

      Stream
        .iterable(requestIds)
        .zipWithIndex
        .flatMap { case (uuid, i) =>
          Stream.eval:
            for
              merged <- getMerged(uuid)
              res <-
                if merged.nonEmpty then
                  getStringData(merged)
                    .flatMap(getHash)
                    .map(hash => Map(SL_ANONYMIZED_FINGERPRINT + i -> hash))
                else F.pure(Map.empty[String, String])
            yield res
        }
        .covary[F]
        .compile
        .toList
