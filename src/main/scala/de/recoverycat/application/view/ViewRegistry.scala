/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application.view

import cats.effect.Async
import cats.effect.kernel.Ref
import de.recoverycat.application.MetricsOps
import de.recoverycat.business.{OutputControl, OutputMessage, TransferContextState, TransferState}
import org.typelevel.log4cats.SelfAwareStructuredLogger

private[view] trait ViewRegistry[F[_]]
    extends ViewCountersAndGauges[F]
    with ViewDuration[F]
    with ViewOutputs[F]

private[view] object ViewRegistry:
  def impl[F[_]: Async](
      transferStateRef: Ref[F, TransferState],
      transferContextStateRef: Ref[F, TransferContextState],
      metricsOps: MetricsOps[F],
      logger: SelfAwareStructuredLogger[F],
  ): ViewRegistry[F] =
    new ViewRegistry[F]:

      private val registerDuration: ViewDuration[F] = ViewDuration.impl(metricsOps)
      private val registerCounters = ViewCountersAndGauges.impl(transferStateRef, metricsOps)
      private val logOutputs = ViewOutputs.impl(transferContextStateRef, logger)

      override def setMetrics(classifier: Option[String]): F[Unit] =
        registerCounters.setMetrics(classifier)

      override def logOutputMessage(outputMessage: OutputMessage): F[Unit] =
        logOutputs.logOutputMessage(outputMessage)

      override def logOutputControl(outputControl: OutputControl): F[Unit] =
        logOutputs.logOutputControl(outputControl)

      override def observeSessionDuration(
          outputControl: OutputControl,
          classifier: Option[String],
      ): F[Unit] = registerDuration.observeSessionDuration(outputControl, classifier)
