/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.effect.kernel.Sync
import de.recoverycat.business.{TransferContextState, TransferContexts, TransferState}
import io.circe.*
import io.circe.generic.semiauto.*

import java.util.UUID

trait SimpleMetrics[F[_]]:

  def build(
      transferState: TransferState,
      transferContextState: TransferContextState,
  ): F[SimpleMetrics.Metrics]

object SimpleMetrics:

  def impl[F[_]](using F: Sync[F]): SimpleMetrics[F] = new SimpleMetrics[F]:
    override def build(
        transferState: TransferState,
        transferContextState: TransferContextState,
    ): F[Metrics] = F.delay:
      val channelRequests = transferState.channelRequestIds.map { case (k, v) =>
        ChannelRequests(k, v, v.size)
      }.toList

      val requestInfo =
        channelRequests.flatMap(_.sessions).map(uuid => RequestInfo(uuid)).map { ri =>
          transferContextState.requestIdTransferContexts
            .getOrElse(ri.requestId, TransferContexts.empty)
            .contexts match
            case Some(value) =>
              value.foldLeft(ri)((m, v) =>
                v match {
                  case context: RequestTransferContext.StartTime =>
                    m.copy(startTime = Some(context.value.toNanos))
                  case RequestTransferContext.EndTime(_) =>
                    // No need to show it as it is state is removed after leaving
                    m
                  case context: RequestTransferContext.Origin =>
                    m.copy(origin = context.value.map(_.head))
                  case context: RequestTransferContext.RemoteAddress =>
                    m.copy(remoteAddress = context.value.map(_.head))
                  case context: RequestTransferContext.UserAgent =>
                    m.copy(userAgent = context.value.map(_.head))
                  case _ => m
                }
              )
            case None => ri

        }

      val channelDataTransfers = transferState.channelDataTransfer.map { case (k, v) =>
        ChannelDataTransfers(k, v)
      }.toList

      Metrics(
        effectiveCompleteChannelsCount = transferState.effectiveCompleteChannelsCount,
        effectiveChannelsCount = transferState.effectiveChannelsCount,
        tmpChannelsCount = transferState.tmpChannelsCount,
        channelRequests = channelRequests,
        requestInfo = requestInfo,
        channelDataTransfers = channelDataTransfers,
      )

  case class ChannelRequests(channelName: String, sessions: Set[UUID], sessionsCount: Int)

  case class RequestInfo(
      requestId: UUID,
      startTime: Option[Long] = None,
      origin: Option[String] = None,
      remoteAddress: Option[String] = None,
      userAgent: Option[String] = None,
  )

  case class ChannelDataTransfers(channelName: String, currentCount: Int)

  case class Metrics(
      effectiveCompleteChannelsCount: Int,
      effectiveChannelsCount: Int,
      tmpChannelsCount: Int,
      channelRequests: List[ChannelRequests],
      requestInfo: List[RequestInfo],
      channelDataTransfers: List[ChannelDataTransfers],
  )

  given Encoder[Metrics] = deriveEncoder[Metrics]
  given Encoder[RequestInfo] = deriveEncoder[RequestInfo]
  given Encoder[ChannelRequests] = deriveEncoder[ChannelRequests]
  given Encoder[ChannelDataTransfers] = deriveEncoder[ChannelDataTransfers]
