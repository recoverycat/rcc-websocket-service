/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.effect.kernel.Ref
import cats.effect.{Async, Clock}
import cats.implicits.*
import de.recoverycat.application.RequestTransferContext.StartTime
import de.recoverycat.business.{Disconnect, InputControl, TransferContextState, TransferState}
import fs2.Stream

import scala.concurrent.duration.{DurationInt, FiniteDuration}

trait TransferStateReviewer[F[_]]:

  def process: Stream[F, Nothing]

object TransferStateReviewer:
  def impl[F[_]: Async](
      transferStateRef: Ref[F, TransferState],
      transferContextStateRef: Ref[F, TransferContextState],
  )(using clock: Clock[F]): TransferStateReviewer[F] = new TransferStateReviewer[F]:

    private final val tickEvery: FiniteDuration = 2.minutes

    private val review = for
      window <- clock.monotonic

      transferState <- transferStateRef.get
      transferContext <- transferContextStateRef.get

      withoutContext = transferState.requestIdChannels
        .filter { case (k, _) =>
          !transferContext.requestIdTransferContexts.contains(k)
        }
        .map { case (k, _) => k }
        .toSet

      withoutStartTime = transferContext.requestIdTransferContexts
        .filter { case (_, v) =>
          !v.exists[StartTime]
        }
        .map { case (k, _) => k }
        .toSet

      withStartTimeAndOld = transferContext.requestIdTransferContexts
        .filter { case (_, v) =>
          v.get[StartTime].map(st => window - st.value).exists(_ >= 2.minutes)
        }
        .map { case (k, _) => k }
        .toSet

      toRemove = withoutContext ++ withoutStartTime ++ withStartTimeAndOld

      _ <- Stream
        .iterable(toRemove)
        .flatMap { uuid =>
          Stream.eval {
            transferContextStateRef.modify { ts =>
              ts.process(InputControl.Remove(uuid))
            }
          } >>
            Stream.eval:
              transferStateRef.modify { ts =>
                ts.process(Disconnect(uuid))
              }
        }
        .covary[F]
        .compile
        .drain
    yield withStartTimeAndOld

    override def process: Stream[F, Nothing] =
      (Stream.awakeEvery[F](tickEvery) >> Stream.eval(review)).drain
