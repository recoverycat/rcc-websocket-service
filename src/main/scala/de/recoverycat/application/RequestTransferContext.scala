/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.data.NonEmptyList
import de.recoverycat.business.{TransferContext, TransferContexts}

import scala.concurrent.duration.FiniteDuration

sealed trait RequestTransferContext extends TransferContext

object RequestTransferContext:

  def calculateElapsedTime(transferContexts: TransferContexts): Option[FiniteDuration] =
    for
      endTime <- transferContexts.get[EndTime].map(_.value)
      startTime <- transferContexts.get[StartTime].map(_.value)
    yield endTime - startTime

  case class StartTime(value: FiniteDuration) extends RequestTransferContext
  case class EndTime(value: FiniteDuration) extends RequestTransferContext
  case class Origin(value: Option[NonEmptyList[String]]) extends RequestTransferContext
  case class RemoteAddress(value: Option[NonEmptyList[String]]) extends RequestTransferContext
  case class UserAgent(value: Option[NonEmptyList[String]]) extends RequestTransferContext
