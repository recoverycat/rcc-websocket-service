/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.application

import cats.effect.kernel.Ref
import cats.effect.std.Queue
import cats.effect.{Async, Clock}
import cats.implicits.*
import de.recoverycat.business.{
  Disconnect,
  InputControl,
  InputMessage,
  InvalidInput,
  JoinChannel,
  KeepAlive,
  OutputControl,
  OutputMessage,
  TransferContextState,
  TransferContexts,
  TransferState,
}
import fs2.concurrent.Topic
import fs2.*

import java.util.UUID
import scala.concurrent.duration.{DurationInt, FiniteDuration}

trait Transfer[F[_]]:

  def send(requestId: UUID): Stream[F, TransferMessage]

  def receive(requestId: UUID): Pipe[F, TransferMessage, Unit]

  def onClose(requestId: UUID): F[Unit]

  def onStart(requestId: UUID, transferContexts: TransferContexts): F[Unit]

  def process: Stream[F, Nothing]

object Transfer:
  def impl[F[_]: Async](
      outputTopic: Topic[F, OutputMessage],
      inputQueue: Queue[F, InputMessage],
      viewQueue: Queue[F, OutputMessage],
      transferContextQueue: Queue[F, OutputControl],
      transferStateRef: Ref[F, TransferState],
      transferContextStateRef: Ref[F, TransferContextState],
      inputMessageParser: InputMessageParser,
      outputMessageParser: OutputMessageParser,
  )(using clock: Clock[F]): Transfer[F] = new Transfer[F]:

    private final val maxQueued: Int = 1000
    private final val keepAliveEvery: FiniteDuration = 30.seconds
    private final val sleepBeforeWelcomeMessage: FiniteDuration = 100.millis

    private final val outputEvents = outputTopic.subscribe(maxQueued)

    def onStart(requestId: UUID, transferContexts: TransferContexts): F[Unit] =

      val initMessageStream = (Stream.sleep(sleepBeforeWelcomeMessage)
        ++
          Stream
            .emit(JoinChannel.withDefaultChannel(requestId = requestId))
            .evalMap(x => inputQueue.offer(x)))

      val transferContextsStream =
        for
          startTime <- Stream.eval(clock.monotonic.map(RequestTransferContext.StartTime.apply))
          updatedTransferContexts <- Stream(transferContexts.append(startTime))
          res <- Stream
            .eval(
              transferContextStateRef.modify(tcs =>
                tcs.process(InputControl.Append(requestId, updatedTransferContexts))
              )
            )
            .flatMap(Stream.emits)
            .evalMap(transferContextQueue.offer)
        yield res

      val res = for
        _ <- transferContextsStream
        res <- initMessageStream
      yield res

      res.covary[F].compile.drain

    override def onClose(requestId: UUID): F[Unit] = {
      val transferContextsStream = for
        endTime <- Stream.eval(clock.monotonic.map(RequestTransferContext.EndTime.apply))
        res <- Stream
          .eval(
            transferContextStateRef.modify(tcs =>
              tcs.process(InputControl.Remove(requestId, TransferContexts.of(endTime)))
            )
          )
          .flatMap(Stream.emits)
          .evalMap(transferContextQueue.offer)
      yield res

      for
        _ <- transferContextsStream
        res <- Stream.exec(inputQueue.offer(Disconnect(requestId)))
      yield res

    }.covary[F].compile.drain

    override def send(requestId: UUID): Stream[F, TransferMessage] =
      for
        msg <- outputEvents.filter(_.forRequestId(requestId))
        maybeTransferMessage = outputMessageParser
          .parseOutput(msg)
          .map(oe => TransferMessage.Text(new String(oe.output))) if maybeTransferMessage.isDefined
      yield maybeTransferMessage.get

    override def receive(requestId: UUID): Pipe[F, TransferMessage, Unit] = { in =>
      for
        msg <- in.collect:
          case TransferMessage.Text(rawText) =>
            InputMessage
              .validate(inputMessageParser.parseInput(requestId, rawText))
              .left
              .map(x => InvalidInput(requestId, x.errorMessage))
              .merge

          case TransferMessage.Close(_) =>
            Disconnect(requestId)
        res <- Stream.eval(inputQueue.offer(msg))
      yield res
    }

    override val process: Stream[F, Nothing] =

      val keepAlive =
        Stream.awakeEvery[F](keepAliveEvery).map(_ => KeepAlive).through(outputTopic.publish)

      val transferStateView = outputEvents.foreach(viewQueue.offer)

      val processing = Stream
        .fromQueueUnterminated(inputQueue)
        .evalMap(msg => transferStateRef.modify(_.process(msg)))
        .flatMap(Stream.emits)
        .through(outputTopic.publish)

      Stream(keepAlive, transferStateView, processing).parJoinUnbounded
