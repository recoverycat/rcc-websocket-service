/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import cats.data.NonEmptyList
import de.recoverycat.application.RequestTransferContext
import de.recoverycat.business.TransferContexts
import org.http4s.Request
import org.typelevel.ci.CIString

private[web] object RequestTransferContextsHelpers:

  def createTransferContexts[F[_]](req: Request[F]): TransferContexts =
    val origin = RequestTransferContext.Origin(
      req.headers.get(CIString("origin")).map(_.map(_.value))
    )
    val remoteAddress = RequestTransferContext.RemoteAddress(
      req.from.map(x => NonEmptyList.one(x.toUriString))
    )
    val userAgent = RequestTransferContext.UserAgent(
      req.headers.get(CIString("user-agent")).map(_.map(_.value))
    )
    TransferContexts(
      Some(NonEmptyList.of(origin, remoteAddress, userAgent))
    )
