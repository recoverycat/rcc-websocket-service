/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import io.circe.DecodingFailure.Reason.WrongTypeExpectation
import io.circe.generic.semiauto.*
import io.circe.parser.decode
import io.circe.{Decoder, DecodingFailure, Encoder, Error}

sealed trait IEdgeInputData

case class IEdgeJoinData(`type`: String, channel: String) extends IEdgeInputData

object IEdgeJoinData:
  given Encoder[IEdgeJoinData] = deriveEncoder
  given Decoder[IEdgeJoinData] = deriveDecoder

case class IEdgePlainData(data: String) extends IEdgeInputData

object IEdgePlainData:
  given Encoder[IEdgePlainData] = deriveEncoder
  given Decoder[IEdgePlainData] =
    Decoder.instance(c => Right(IEdgePlainData(c.value.noSpaces)))

object IEdgeInputData:

  given (using
      encoderIEdgeJoinData: Encoder[IEdgeJoinData],
      encoderIEdgePlainData: Encoder[IEdgePlainData],
  ): Encoder[IEdgeInputData] =
    Encoder.instance {
      case rawJoin: IEdgeJoinData => encoderIEdgeJoinData(rawJoin)
      case rawData: IEdgePlainData => encoderIEdgePlainData(rawData)
    }

  given (using decoderEdgePlainData: Decoder[IEdgePlainData]): Decoder[IEdgeInputData] =
    Decoder
      .instance { cursor =>
        cursor
          .get[String]("type")
          .flatMap:
            case "join" => cursor.as[IEdgeJoinData]
            case _ =>
              Left(
                DecodingFailure(
                  WrongTypeExpectation("type not found", cursor.value),
                  cursor.history,
                )
              )
          .map(_.asInstanceOf[IEdgeInputData])
      }
      .handleErrorWith { _ =>
        decoderEdgePlainData.map(_.asInstanceOf[IEdgeInputData])
      }

  def parseInput(rawText: String): Either[Error, IEdgeInputData] =
    val trimmedRawText = rawText.trim

    trimmedRawText.take(1) match
      case "{" => decode(trimmedRawText)
      case "[" => decode(trimmedRawText)
      case "\"" => decode(trimmedRawText)
      case _ => Right(IEdgePlainData(trimmedRawText))
