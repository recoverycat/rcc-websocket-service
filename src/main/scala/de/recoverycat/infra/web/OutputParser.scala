/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import de.recoverycat.application.OutputMessageParser
import de.recoverycat.business.{
  AlreadyInChannel,
  ChannelJoined,
  InvalidInputMessage,
  KeepAlive,
  LeftChannel,
  OEdgeMessage,
  OutputDataTransferData,
  OutputMessage,
  OutputMessageTargeted,
  Signal,
  WelcomeMessage,
}

import java.nio.charset.StandardCharsets

object OutputParser:

  def impl: OutputMessageParser = new OutputMessageParser:
    override def parseOutput(outputMessage: OutputMessage): Option[OEdgeMessage] =
      outputMessage match
        case targeted: OutputMessageTargeted =>
          val res = targeted match
            case WelcomeMessage(_, _) =>
              OEdgeSimpleMessage(
                "connect",
                "You are now connected to the Recovery Cat's Signaling Server",
                isError = false,
              )
            case LeftChannel(_, _, message) =>
              OEdgeSimpleMessage("left-channel", message, isError = false)
            case ChannelJoined(_, _, self, channel, count, message) =>
              OEdgeChannelJoined("joined", self, channel, count, message)
            case OutputDataTransferData(_, _, _, data) =>
              OEdgeBinaryMessage(data.getBytes(StandardCharsets.UTF_8))
            case KeepAlive =>
              OEdgeSimpleMessage("keep-alive", "this is heartbeat", isError = false)
            case InvalidInputMessage(_, _, message) =>
              OEdgeSimpleMessage("invalid-input", message, isError = true)
            case AlreadyInChannel(_, _, _, message) =>
              OEdgeSimpleMessage("already-in-channel", message, isError = true)
          Some(res)
        case _: Signal => None
