/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import de.recoverycat.application.TransferMessage
import org.http4s.websocket.WebSocketFrame

trait TransferMessageAdaptor:
  def toTransferMessage(webSocketFrame: WebSocketFrame): TransferMessage
  def toWebsocketFrame(transferMessage: TransferMessage): WebSocketFrame

object TransferMessageAdaptor:
  def impl: TransferMessageAdaptor = new TransferMessageAdaptor:
    override def toTransferMessage(webSocketFrame: WebSocketFrame): TransferMessage =
      webSocketFrame match
        case WebSocketFrame.Text(str, _) => TransferMessage.Text(str)
        case WebSocketFrame.Close(data) => TransferMessage.Close(data)
        case other => TransferMessage.Unknown(other.data)

    override def toWebsocketFrame(transferMessage: TransferMessage): WebSocketFrame =
      transferMessage match
        case TransferMessage.Text(str) => WebSocketFrame.Text(str)
        case TransferMessage.Close(data) => WebSocketFrame.Close(data)
        case TransferMessage.Unknown(data) => WebSocketFrame.Close(data)
