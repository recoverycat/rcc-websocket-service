/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import cats.effect.*
import org.http4s.*
import org.http4s.metrics.prometheus.{Prometheus, PrometheusExportService}
import org.http4s.server.Router
import org.http4s.server.middleware.Metrics

object PrometheusMiddleware:

  def meteredRouter[F[_]: Async](
      metricsSvc: PrometheusExportService[F],
      prefix: String,
      routes: HttpRoutes[F],
  ): Resource[F, HttpRoutes[F]] =
    for
      metrics <- Prometheus.metricsOps[F](metricsSvc.collectorRegistry, prefix)
      router = Router[F](
        "/api" -> Metrics[F](metrics)(routes),
        "/" -> metricsSvc.routes,
      )
    yield router
