/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import de.recoverycat.business.OEdgeMessage
import io.circe.*
import io.circe.generic.semiauto.*
import io.circe.syntax.*

import java.nio.charset.StandardCharsets

case class OEdgeSimpleMessage(`type`: String, message: String, isError: Boolean)
    extends OEdgeMessage:

  def output: Array[Byte] = OEdgeSimpleMessage.json(this).getBytes(StandardCharsets.UTF_8)

object OEdgeSimpleMessage:
  given Decoder[OEdgeSimpleMessage] = deriveDecoder[OEdgeSimpleMessage]
  given Encoder[OEdgeSimpleMessage] = deriveEncoder[OEdgeSimpleMessage]

  def json(oedge: OEdgeSimpleMessage): String = oedge.asJson.noSpaces

case class OEdgeBinaryMessage(data: Array[Byte]) extends OEdgeMessage:
  def output: Array[Byte] =
    data

case class OEdgeChannelJoined(
    `type`: String,
    self: Boolean,
    channel: String,
    count: Int,
    message: String,
) extends OEdgeMessage:

  def output: Array[Byte] = OEdgeChannelJoined.json(this).getBytes(StandardCharsets.UTF_8)

object OEdgeChannelJoined:
  given Decoder[OEdgeChannelJoined] = deriveDecoder[OEdgeChannelJoined]
  given Encoder[OEdgeChannelJoined] = deriveEncoder[OEdgeChannelJoined]

  def json(oedge: OEdgeChannelJoined): String = oedge.asJson.noSpaces
