/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import cats.effect.Async
import cats.effect.implicits.genSpawnOps
import cats.effect.kernel.Ref
import cats.effect.std.UUIDGen
import cats.implicits.*
import de.recoverycat.application.{SimpleMetrics, Transfer}
import de.recoverycat.business.{TransferContextState, TransferState}
import io.circe.syntax.*
import fs2.*
import org.http4s.HttpRoutes
import org.http4s.circe.streamJsonArrayEncoder
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.Connection
import org.http4s.server.websocket.WebSocketBuilder2
import org.http4s.websocket.WebSocketFrame

object Routes:

  def simpleMetrics[F[_]: Async](
      simpleMetrics: SimpleMetrics[F],
      transferState: Ref[F, TransferState],
      transferContextState: Ref[F, TransferContextState],
  ): HttpRoutes[F] =
    val dsl = new Http4sDsl[F] {}
    import dsl.*
    HttpRoutes.of[F] { case GET -> Root / "metrics" =>
      val outputStream =
        for
          transferState <- Stream.eval(transferState.get)
          transferContextState <- Stream.eval(transferContextState.get)
          metrics <- Stream.eval(simpleMetrics.build(transferState, transferContextState))
        yield metrics.asJson

      Ok(outputStream)
    }

  def websocketRoutes[F[_]: Async](
      ws: WebSocketBuilder2[F],
      T: Transfer[F],
      TAdaptor: TransferMessageAdaptor,
  ): HttpRoutes[F] =
    val dsl = new Http4sDsl[F] {}
    import dsl.*
    HttpRoutes.of[F] { case req @ GET -> Root =>
      for
        requestId <- UUIDGen.randomUUID[F]

        res <-
          val send: Stream[F, WebSocketFrame] = T.send(requestId).map(TAdaptor.toWebsocketFrame)

          val receive: Pipe[F, WebSocketFrame, Unit] = { in =>
            T.receive(requestId):
              in.map(TAdaptor.toTransferMessage)
          }

          ws.withOnClose(T.onClose(requestId)).build(send, receive)

        _ <- req.headers.get[Connection] match
          case Some(header) if header.hasUpgrade =>
            T.onStart(requestId, RequestTransferContextsHelpers.createTransferContexts(req))
              .start
              .as(())
          case _ => ().pure[F]
      yield res
    }
