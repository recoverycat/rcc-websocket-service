/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.web

import de.recoverycat.application.InputMessageParser
import de.recoverycat.business.{InputMessage, InvalidInput, JoinChannel, TransferData}

import java.util.UUID

object InputParser:
  def impl: InputMessageParser = new InputMessageParser:
    override def parseInput(requestId: UUID, rawText: String): InputMessage =
      if rawText.isEmpty then InvalidInput(requestId, "Empty input not permitted")
      else
        IEdgeInputData
          .parseInput(rawText)
          .map:
            case IEdgeJoinData(_, channel) => JoinChannel(requestId, channel)
            case IEdgePlainData(data) => TransferData(requestId, data)
        match
          case Left(value) => InvalidInput(requestId, text = value.getMessage)
          case Right(value: InputMessage) => value
