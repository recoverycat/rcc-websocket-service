/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.infra.prometheus

import cats.effect.{Resource, Sync}
import cats.implicits.*
import de.recoverycat.application.MetricsOps
import io.prometheus.client.{Collector, CollectorRegistry, Gauge, Histogram}

final case class MetricsCollection(
    channelsGauge: Gauge,
    sessionDurationHistogram: Histogram,
)

object PrometheusMetrics:

  def metricsOps[F[_]: Sync](
      registry: CollectorRegistry,
      prefix: String,
  ): Resource[F, MetricsOps[F]] =
    for metrics <- createMetricsCollection(registry, prefix)
    yield createMetricsOps(metrics)

  private def createMetricsCollection[F[_]: Sync](
      registry: CollectorRegistry,
      prefix: String,
  ): Resource[F, MetricsCollection] =

    val channelsGauge: Resource[F, Gauge] = registerCollector(
      Gauge
        .build()
        .name(prefix + "_" + "channels_gauge")
        .help("Total Complete Active Channels: Number of participants has been reached")
        .labelNames("classifier")
        .create(),
      registry,
    )

    val sessionDurationHistogram: Resource[F, Histogram] = registerCollector(
      Histogram
        .build()
        .name(prefix + "_" + "session_duration")
        .help("Session duration time")
        .labelNames("classifier")
        .create(),
      registry,
    )

    (
      channelsGauge,
      sessionDurationHistogram,
    ).mapN(
      MetricsCollection.apply
    )

  private def registerCollector[F[_], C <: Collector](
      collector: C,
      registry: CollectorRegistry,
  )(using F: Sync[F]): Resource[F, C] =
    Resource.make(F.blocking(collector.register[C](registry)))(c =>
      F.blocking(registry.unregister(c))
    )

  private def createMetricsOps[F[_]](
      metrics: MetricsCollection
  )(using F: Sync[F]): MetricsOps[F] =
    new MetricsOps[F]:

      override def setChannels(channels: Int, classifier: Option[String]): F[Unit] =
        F.delay:
          metrics.channelsGauge
            .labels(label(classifier))
            .set(channels.toDouble)

      override def observeSessionDuration(elapsed: Long, classifier: Option[String]): F[Unit] =
        F.delay:
          metrics.sessionDurationHistogram
            .labels(label(classifier))
            .observe(elapsed.toDouble)

  private def label(value: Option[String]): String = value.getOrElse("")
