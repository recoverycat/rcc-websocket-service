/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

sealed trait DomainValidation:
  def errorMessage: String

case object ChannelNameValidation extends DomainValidation:
  final val minLength = 5
  final val maxLength = 30
  final val blackSpaces = false

  def errorMessage: String =
    s"Channel name is required and should be $minLength >= length <= $maxLength"

case object DataValidation extends DomainValidation:
  def errorMessage: String = "Data is wrong"

case object InvalidMessageValidation extends DomainValidation:
  def errorMessage: String = "Invalid message validation"

trait JoinChannelValidator:

  private def validateChannelName(channelName: String): Either[DomainValidation, String] =
    Either.cond(
      channelName.nonEmpty &&
        channelName.length >= ChannelNameValidation.minLength &&
        channelName.length <= ChannelNameValidation.maxLength &&
        (if ChannelNameValidation.blackSpaces then true else !channelName.contains(" ")),
      channelName,
      ChannelNameValidation,
    )

  def validate(joinChannel: JoinChannel): Either[DomainValidation, JoinChannel] =
    for validatedChannel <- validateChannelName(joinChannel.channel)
    yield JoinChannel(joinChannel.requestId, validatedChannel)

trait TransferDataValidator:

  private def validateData(data: String): Either[DomainValidation, String] =
    Either.cond(
      data.nonEmpty,
      data,
      DataValidation,
    )

  def validate(transferData: TransferData): Either[DomainValidation, TransferData] =
    for validatedData <- validateData(transferData.data)
    yield TransferData(transferData.requestId, validatedData)

trait DisconnectValidator:

  def validate(disconnect: Disconnect): Either[DomainValidation, Disconnect] = Right(disconnect)

trait InvalidInputValidator:

  private def validateInvalidInputMessage(data: String): Either[DomainValidation, String] =
    Either.cond(
      data.nonEmpty,
      data,
      InvalidMessageValidation,
    )

  def validate(invalidInput: InvalidInput): Either[DomainValidation, InvalidInput] =
    for validatedText <- validateInvalidInputMessage(invalidInput.text)
    yield InvalidInput(invalidInput.requestId, validatedText)

object JoinChannelValidator extends JoinChannelValidator

object TransferDataValidator extends TransferDataValidator

object DisconnectValidator extends DisconnectValidator

object InvalidInputValidator extends InvalidInputValidator
