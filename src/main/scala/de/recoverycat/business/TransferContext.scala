/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import cats.data.NonEmptyList

import scala.reflect.ClassTag

trait TransferContext

case class TransferContexts(contexts: Option[NonEmptyList[TransferContext]]):

  def isEmpty: Boolean = contexts.isEmpty

  def append(transferContext: TransferContext): TransferContexts =
    val newContexts =
      contexts.map(_.append(transferContext)).orElse(Some(NonEmptyList.one(transferContext)))
    copy(contexts = newContexts)

  def merge(other: TransferContexts): TransferContexts =
    val mergedContexts = (contexts, other.contexts) match
      case (Some(existing), Some(additional)) => Some(existing.concatNel(additional))
      case (existing, additional) => existing.orElse(additional)
    copy(contexts = mergedContexts)

  def get[T <: TransferContext: ClassTag]: Option[T] =
    contexts.flatMap:
      _.collectFirst:
        case x if implicitly[ClassTag[T]].runtimeClass.isInstance(x) =>
          x.asInstanceOf[T]

  def exists[T <: TransferContext: ClassTag]: Boolean = get[T].isDefined

  def filter(transferContextF: TransferContext => Boolean): Option[List[TransferContext]] =
    contexts.map:
      _.filter(transferContextF)

object TransferContexts:
  def empty: TransferContexts = TransferContexts(None)
  def of(transferContext: TransferContext): TransferContexts = empty.append(transferContext)
