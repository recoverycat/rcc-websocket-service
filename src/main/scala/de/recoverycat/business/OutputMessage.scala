/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import java.util.UUID

sealed trait OutputMessage:

  def forRequestId(targetRequestId: UUID): Boolean

sealed trait OutputMessageTargeted extends OutputMessage:
  def to: Option[UUID]

  def from: Option[UUID]

  def forRequestId(targetRequestId: UUID): Boolean = to == Option(targetRequestId)

case class WelcomeMessage(to: Option[UUID], from: Option[UUID] = None) extends OutputMessageTargeted

case class LeftChannel(to: Option[UUID], from: Option[UUID] = None, message: String)
    extends OutputMessageTargeted

case class ChannelJoined(
    to: Option[UUID],
    from: Option[UUID] = None,
    self: Boolean,
    channel: String,
    count: Int,
    message: String,
) extends OutputMessageTargeted

case class OutputDataTransferData(
    to: Option[UUID],
    from: Option[UUID] = None,
    channel: Option[String],
    data: String,
) extends OutputMessageTargeted

case object KeepAlive extends OutputMessageTargeted:

  override def to: Option[UUID] = None

  override def from: Option[UUID] = None

  override def forRequestId(targetRequestId: UUID) = true

case class InvalidInputMessage(to: Option[UUID], from: Option[UUID] = None, message: String)
    extends OutputMessageTargeted

case class AlreadyInChannel(
    to: Option[UUID],
    from: Option[UUID] = None,
    channel: String,
    message: String,
) extends OutputMessageTargeted

sealed trait Signal extends OutputMessage:
  override def forRequestId(targetRequestId: UUID): Boolean = false

object Signal:
  case class DefaultChannelSignal(requestId: UUID, channel: String) extends Signal
  case class ChannelFirstJoinSignal(requestId: UUID, channel: String) extends Signal
  case class ChannelCompletedSignal(requestIds: Set[UUID], channel: String) extends Signal
  case class GlobalDataTransferSignal(requestIds: Set[UUID], channel: String) extends Signal
  case class TotalDataTransferSignal(requestIds: Set[UUID], channel: String, current: Int)
      extends Signal
  case class LeftChannelSignal(requestId: UUID, channel: String) extends Signal
