/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import java.util.UUID

sealed trait InputControl:
  val requestId: UUID

object InputControl:
  case class Append(requestId: UUID, transferContexts: TransferContexts) extends InputControl
  case class Remove(requestId: UUID, transferContexts: TransferContexts) extends InputControl
  object Remove:
    def apply(requestId: UUID): Remove = new Remove(requestId, TransferContexts.empty)
