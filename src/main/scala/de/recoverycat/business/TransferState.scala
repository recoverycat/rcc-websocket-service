/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import java.util.UUID

object TransferState:

  def empty: TransferState = TransferState(Map.empty, Map.empty, Map.empty)

case class TransferState(
    requestIdChannels: Map[UUID, String],
    channelRequestIds: Map[String, Set[UUID]],
    channelDataTransfer: Map[String, Int],
):

  private final val maxSessionsPerChannel = 2

  def effectiveCompleteChannelsCount: Int = channelRequestIds.count { case (k, v) =>
    !JoinChannel.matchesDefaultChannel(k) && v.size == maxSessionsPerChannel
  }

  def effectiveChannelsCount: Int = channelRequestIds.count { case (k, v) =>
    !JoinChannel.matchesDefaultChannel(k) && v.size < maxSessionsPerChannel
  }

  def tmpChannelsCount: Int = channelRequestIds.count { case (k, _) =>
    JoinChannel.matchesDefaultChannel(k)
  }

  def process(msg: InputMessage): (TransferState, Seq[OutputMessage]) =
    msg match

      case JoinChannel(requestId, channelToJoin)
          if getChannelRequestIds(channelToJoin).size == maxSessionsPerChannel =>
        (
          this,
          Seq(
            AlreadyInChannel(
              to = Option(requestId),
              from = Option(requestId),
              channel = channelToJoin,
              message = "Channel is full",
            )
          ),
        )

      case joinChannel @ JoinChannel(requestId, channelToJoin) =>
        requestIdChannels.get(requestId) match

          case None if JoinChannel.matchesDefaultChannel(joinChannel) =>
            val (finalState, _) = addToChannel(requestId, channelToJoin)

            (
              finalState,
              Seq(
                WelcomeMessage(to = Some(requestId), from = Some(requestId)),
                Signal.DefaultChannelSignal(
                  requestId = requestId,
                  channel = channelToJoin,
                ),
              ),
            )

          case None =>
            val (finalState, enterMessages) = addToChannel(requestId, channelToJoin)

            (finalState, enterMessages)

          case Some(currentChannel) if currentChannel == channelToJoin =>
            (
              this,
              Seq(
                AlreadyInChannel(
                  to = Option(requestId),
                  from = Option(requestId),
                  channel = channelToJoin,
                  message = "You are already in that channel!",
                )
              ),
            )

          case Some(_) if JoinChannel.matchesDefaultChannel(joinChannel) =>
            (this, Nil)

          case Some(_) =>
            val (intermediateState, leaveMessages, _) = removeFromCurrentChannel(requestId)
            val (finalState, enterMessages) =
              intermediateState.addToChannel(requestId, channelToJoin)

            (finalState, leaveMessages ++ enterMessages)

      case TransferData(requestId, data) =>
        val maybeChannel = this.requestIdChannels.get(requestId)
        val nextStateSessions = maybeChannel
          .map(getChannelRequestIds)
          .getOrElse(Set.empty)

        if nextStateSessions.size < maxSessionsPerChannel then
          process(InvalidInput(requestId = requestId, text = "Invalid number of partners"))
        else

          val nextStateTransferData = maybeChannel
            .map { channel =>
              Map(channel -> (this.channelDataTransfer.getOrElse(channel, 0) + 1))
            }
            .getOrElse(Map.empty)

          val dataTransferSignal = maybeChannel
            .map { channel =>
              val count = nextStateTransferData.getOrElse(channel, 0)
              if count > 1 then
                Set(Signal.TotalDataTransferSignal(nextStateSessions, channel, count))
              else
                Set(
                  Signal.GlobalDataTransferSignal(nextStateSessions, channel),
                  Signal.TotalDataTransferSignal(nextStateSessions, channel, count),
                )
            }
            .getOrElse(Set.empty)

          val nextState = TransferState(
            requestIdChannels = requestIdChannels,
            channelRequestIds = channelRequestIds,
            channelDataTransfer = channelDataTransfer ++ nextStateTransferData,
          )

          (
            nextState,
            nextStateSessions
              .filterNot(_ == requestId)
              .map { reqId =>
                OutputDataTransferData(
                  Some(reqId),
                  from = Some(requestId),
                  channel = maybeChannel,
                  data = data,
                )
              }
              .toSeq ++
              dataTransferSignal,
          )

      case Disconnect(requestId) =>
        val (finalState, leaveMessages, maybeChannel) = removeFromCurrentChannel(requestId)

        val leftChannelSignal = maybeChannel
          .map { channel =>
            Seq(Signal.LeftChannelSignal(requestId = requestId, channel = channel))
          }
          .getOrElse(Seq.empty)

        (
          finalState,
          leaveMessages ++ leftChannelSignal,
        )

      case InvalidInput(requestId, text) =>
        (
          this,
          Seq(
            InvalidInputMessage(
              to = Some(requestId),
              from = Option(requestId),
              message = s"Invalid input: $text",
            )
          ),
        )

  private def removeFromCurrentChannel(
      requestId: UUID
  ): (TransferState, Seq[OutputMessage], Option[String]) =
    requestIdChannels.get(requestId) match
      case Some(channel) =>
        val nextUsers = getChannelRequestIds(channel) - requestId

        val nextState =
          if nextUsers.isEmpty then
            TransferState(
              requestIdChannels - requestId,
              channelRequestIds - channel,
              channelDataTransfer - channel,
            )
          else
            TransferState(
              requestIdChannels - requestId,
              channelRequestIds + (channel -> nextUsers),
              channelDataTransfer,
            )

        val nextStateSessions = nextState.getChannelRequestIds(channel)

        (
          nextState,
          nextStateSessions
            .map(reqId =>
              LeftChannel(to = Some(reqId), from = Some(requestId), message = "Left channel")
            )
            .toSeq,
          Some(channel),
        )

      case None =>
        (this, Nil, None)

  private def addToChannel(
      requestId: UUID,
      channel: String,
  ): (TransferState, Seq[OutputMessage]) =
    val nextUsers = getChannelRequestIds(channel) + requestId
    val nextState =
      TransferState(
        requestIdChannels + (requestId -> channel),
        channelRequestIds + (channel -> nextUsers),
        channelDataTransfer + (channel -> 0),
      )

    val nextStateSessions = nextState.getChannelRequestIds(channel)

    val channelFirstJoinSignal =
      if nextStateSessions.size < maxSessionsPerChannel then
        Seq(
          Signal
            .ChannelFirstJoinSignal(requestId, channel = channel)
        )
      else Seq.empty

    val channelCompleteSignal =
      if nextStateSessions.size == maxSessionsPerChannel then
        Seq(
          Signal
            .ChannelCompletedSignal(nextUsers, channel = channel)
        )
      else Seq.empty

    val addMessages = nextStateSessions
      .flatMap(reqId =>
        Seq(
          ChannelJoined(
            to = Some(reqId),
            from = Some(requestId),
            self = requestId == reqId,
            channel = channel,
            count = nextStateSessions.size,
            message = s"listening in $channel",
          )
        )
      )
      .toSeq ++ channelCompleteSignal ++ channelFirstJoinSignal

    (
      nextState,
      addMessages,
    )

  private def getChannelRequestIds(channel: String): Set[UUID] =
    channelRequestIds.getOrElse(channel, Set.empty)
