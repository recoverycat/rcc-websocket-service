/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import java.util.UUID

object TransferContextState:

  def empty: TransferContextState = TransferContextState(Map.empty)

case class TransferContextState(requestIdTransferContexts: Map[UUID, TransferContexts]):

  private def append(
      requestId: UUID,
      transferContexts: TransferContexts,
  ): Map[UUID, TransferContexts] =
    val existingTransferContextsForRequestId =
      requestIdTransferContexts.getOrElse(requestId, TransferContexts.empty)

    if existingTransferContextsForRequestId.isEmpty then
      requestIdTransferContexts + (requestId -> transferContexts)
    else
      requestIdTransferContexts + (requestId -> existingTransferContextsForRequestId.merge(
        transferContexts
      ))

  def process(inputControl: InputControl): (TransferContextState, Seq[OutputControl]) =
    inputControl match
      case InputControl.Append(requestId, transferContexts) =>
        val newContexts = append(requestId, transferContexts)
        val newContextsForRequestId = newContexts.getOrElse(requestId, TransferContexts.empty)

        (
          TransferContextState(newContexts),
          Seq(OutputControl.AppendInfo(requestId, newContextsForRequestId)),
        )

      case InputControl.Remove(requestId, transferContexts) =>
        val newContexts = append(requestId, transferContexts)
        val newContextsForRequestId = newContexts.getOrElse(requestId, TransferContexts.empty)

        (
          TransferContextState(newContexts - requestId),
          Seq(OutputControl.RemoveInfo(requestId, newContextsForRequestId)),
        )
