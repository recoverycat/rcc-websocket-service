/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat.business

import java.util.UUID

sealed trait InputMessage:
  val requestId: UUID

object InputMessage:

  def validate(inputMessage: InputMessage): Either[DomainValidation, InputMessage] =
    inputMessage match
      case obj: JoinChannel => JoinChannelValidator.validate(obj)
      case obj: TransferData => TransferDataValidator.validate(obj)
      case obj: Disconnect => DisconnectValidator.validate(obj)
      case obj: InvalidInput => InvalidInputValidator.validate(obj)

case class JoinChannel(requestId: UUID, channel: String) extends InputMessage
object JoinChannel:
  private final val DEFAULT_CHANNEL_PREFIX: String = "_tmp"

  final def defaultChannel(requestId: UUID): String = requestId.toString + DEFAULT_CHANNEL_PREFIX

  final def matchesDefaultChannel(channel: String): Boolean =
    channel.endsWith(JoinChannel.DEFAULT_CHANNEL_PREFIX)

  final def matchesDefaultChannel(requestId: UUID, channel: String): Boolean =
    JoinChannel.defaultChannel(requestId) == channel

  final def matchesDefaultChannel(joinChannel: JoinChannel): Boolean =
    JoinChannel.matchesDefaultChannel(joinChannel.requestId, joinChannel.channel)

  def withDefaultChannel(requestId: UUID): JoinChannel =
    JoinChannel(requestId = requestId, channel = defaultChannel(requestId))

case class TransferData(requestId: UUID, data: String) extends InputMessage
case class Disconnect(requestId: UUID) extends InputMessage
case class InvalidInput(requestId: UUID, text: String) extends InputMessage
