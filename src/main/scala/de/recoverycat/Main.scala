/*
 * Copyright 2023 Recovery Cat GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.recoverycat

import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.*

import scala.concurrent.duration.{Duration, DurationInt}

object Main extends IOApp:

  override def run(args: List[String]): IO[ExitCode] =

    val websocketHost: Host = ipv4"0.0.0.0"
    val websocketPort: Port = port"8080"
    val websocketIdleTimeout: Duration = 60.seconds

    val webAppHost: Host = ipv4"0.0.0.0"
    val webAppPort: Port = port"8081"
    val webAppIdleTimeout: Duration = 60.seconds

    val metricsPrefix: String = "rcc"

    Service.run[IO](
      websocketHost,
      websocketPort,
      websocketIdleTimeout,
      webAppHost,
      webAppPort,
      webAppIdleTimeout,
      metricsPrefix,
    )
