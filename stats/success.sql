WITH results AS
         (SELECT origin,

                 (WITH success AS
                           (SELECT t."rcc.wss.origin_0",
                                   t."rcc.wss.origin_1",
                                   t."rcc.wss.channel",
                                   EXISTS
                                       (SELECT 1
                                        FROM transfers
                                        WHERE "rcc.wss.channel" =
                                              substr(t."rcc.wss.channel", 2, length(t."rcc.wss.channel")) ||
                                              substr(t."rcc.wss.channel", 1, 1))                           AS repeated_carried_over_left,
                                   EXISTS
                                       (SELECT 1
                                        FROM transfers
                                        WHERE "rcc.wss.channel" =
                                              substr(t."rcc.wss.channel", length(t."rcc.wss.channel"),
                                                     length(t."rcc.wss.channel")) ||
                                              substr(t."rcc.wss.channel", 0, length(t."rcc.wss.channel"))) AS repeated_carried_over_right
                            FROM transfers AS t
                            WHERE "rcc.wss.origin_0" not like '%.qa.%'
                              AND "rcc.wss.origin_0" not like '%.dev.%'
                              AND "rcc.wss.origin_0" not like '%localhost%'
                              AND "rcc.wss.origin_1" not like '%.qa.%'
                              AND "rcc.wss.origin_1" not like '%.dev.%'
                              AND "rcc.wss.origin_1" not like '%localhost%'
                              AND repeated_carried_over_left = 0               -- perfect transmission
                              AND repeated_carried_over_right = 0              -- perfect transmission
                              AND ("rcc.wss.origin_0" = combined_origin.origin
                                OR "rcc.wss.origin_1" = combined_origin.origin)
                              AND t."rcc.wss.origin_0" != t."rcc.wss.origin_1" -- backups
                            GROUP BY "rcc.wss.channel")
                  SELECT count(*)
                  FROM success)          AS success,

                 (WITH success_filtered AS
                           (SELECT t."rcc.wss.origin_0",
                                   t."rcc.wss.origin_1",
                                   t."rcc.wss.channel",
                                   EXISTS
                                       (SELECT 1
                                        FROM transfers
                                        WHERE "rcc.wss.channel" =
                                              substr(t."rcc.wss.channel", length(t."rcc.wss.channel"),
                                                     length(t."rcc.wss.channel")) ||
                                              substr(t."rcc.wss.channel", 0, length(t."rcc.wss.channel"))) AS repeated_carried_over_right
                            FROM transfers AS t
                            WHERE "rcc.wss.origin_0" not like '%.qa.%'
                              AND "rcc.wss.origin_0" not like '%.dev.%'
                              AND "rcc.wss.origin_0" not like '%localhost%'
                              AND "rcc.wss.origin_1" not like '%.qa.%'
                              AND "rcc.wss.origin_1" not like '%.dev.%'
                              AND "rcc.wss.origin_1" not like '%localhost%'
                              AND repeated_carried_over_right = 1              -- shifted channel
                              AND ("rcc.wss.origin_0" = combined_origin.origin
                                OR "rcc.wss.origin_1" = combined_origin.origin)
                              AND t."rcc.wss.origin_0" != t."rcc.wss.origin_1" -- backups
                            GROUP BY "rcc.wss.channel")
                  SELECT count(*)
                  FROM success_filtered) AS success_with_retry
          FROM (SELECT "rcc.wss.origin_0" AS origin
                FROM transfers
                UNION ALL
                SELECT transfers."rcc.wss.origin_1" AS origin
                FROM transfers) AS combined_origin
          GROUP BY origin
          ORDER BY origin)
SELECT origin,
       success,
       success_with_retry,
       success + success_with_retry AS total
FROM results
WHERE origin like 'https://hcp.%' -- We only need half of the results.
  AND total > 0
ORDER BY total DESC;
