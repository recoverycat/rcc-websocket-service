#!/bin/bash

DB_NAME='output.db1'

FILE_DATE='dates.sql'
FILE_DATE_QUERY=$(<"$FILE_DATE")

FILE_QUERY='success.sql'
QUERY=$(<"$FILE_QUERY")

# Check if CSV_FILE_PATH is provided as a command-line argument
if [ $# -eq 0 ]; then
  echo "Usage: $0 CSV_FILE_PATH"
  exit 1
fi

CSV_FILE_PATH="$1" # Assign the first command-line argument to CSV_FILE_PATH

sqlite3 "$DB_NAME" ".mode csv" ".headers ON" ".mode columns" ".import -v --csv '$CSV_FILE_PATH' transfers" "$FILE_DATE_QUERY" "$QUERY"

rm $DB_NAME
