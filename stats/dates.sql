SELECT (SELECT time AS start_time
FROM (SELECT strftime('%Y-%m-%d', TIME) AS time
    FROM transfers
    GROUP BY time
    ORDER BY time)
WHERE time IS NOT NULL
GROUP BY time
ORDER BY time
    LIMIT 1) AS start_time,
    (
SELECT time AS end_time
FROM (SELECT strftime('%Y-%m-%d', time) AS time
    FROM transfers
    GROUP BY time
    ORDER BY time)
WHERE time IS NOT NULL
GROUP BY time
ORDER BY time DESC
    LIMIT 1) as end_time;
